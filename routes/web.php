<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Imgfly::routes();

Route::get('/','SiteController@index');
Route::get('student-register','SiteController@student_reg');
Route::get('teacher-register','SiteController@teacher_reg');
// Route::get('city/{slug}','SiteController@index');
// Route::get('page/{any}','SiteController@page');
// Route::get('program-and-fees','SiteController@programme');
// Route::get('program-and-fees/{any}/{step}','SiteController@programme');
// Route::get('change-location','SiteController@location');
// Route::get('contact-us','SiteController@contact');
// Route::get('location/{any}','SiteController@location2');
// Route::get('register/{any}','SiteController@location3');
// Route::get('inclass-online-classroom-available','SiteController@vclass');
// Route::get('report','SiteController@report');

Route::post('student_reg_form', 'AjaxController@student_reg_form');
Route::post('teacher_reg_form', 'AjaxController@teacher_reg_form');

// Route::post('profile_type', 'AjaxController@profile_type');
// Route::post('contact_form', 'AjaxController@contact_form');
// Route::post('programm_form', 'AjaxController@programm_form');
// Route::post('schedule_form', 'AjaxController@schedule_form');
// Route::post('proccess_form', 'AjaxController@proccess_form');
// Route::get('client-log','AjaxController@clienLog');

// Route::post('stripe', 'OrderController@stripePost')->name('stripe.post');
// Route::post('process', 'OrderController@chequeCash')->name('payment.process');
// Route::get('payment-pending','OrderController@paymentPending');
// Route::get('payment-done','OrderController@paymentDone');
// Route::get('payment-failed','OrderController@paymentFailed');
// Route::get('report2','OrderController@report');

// Route::get("download-pdf","SiteController@downloadPDF");
// Route::get("secure-form","SiteController@secure_form");
// Route::post("secure-form","SiteController@secure_form");

// Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
// Route::post('login', 'Auth\LoginController@login');
// Route::get('logout', 'Auth\LoginController@logout')->name('logout');

// // Registration Routes...
// Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
// Route::post('register', 'Auth\RegisterController@register');


// Route::get('faq','HomeController@faqPage');
// Route::get('page/{any}','HomeController@page');


// // Password Reset Routes...
// Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
// Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
// Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
// Route::post('password/reset', 'Auth\ResetPasswordController@reset');
// Route::get('dashboard','UserController@dashboard');
// Route::get('saveWishlist/{tour_id}','UserController@saveWishlist');
// Route::get('wishlist','UserController@wishlist');
// Route::get('/user/edit_profile/{id}','UserController@editProfile');
// Route::post('/user/saveEditProfile','UserController@saveEditProfile');
// Route::post('/user/savePassword','UserController@savePassword');

// Route::get('/redirect/{service}','SocialAuthController@redirect');
// Route::get('/callback/{service}','SocialAuthController@callback');
// //Route::get('home')

// //TourController
// Route::get('/','TourController@index');
// Route::get('location/{keyword}','TourController@listingTour');
// Route::get('attractions/{keyword}','TourController@listingTour');
// Route::get('tour/{slug}','TourController@detailTour');
// Route::get('tours','TourController@tours');
// Route::get('category/{any}','TourController@category');
// Route::post('saveReview','TourController@saveReview');

// Route::post('order/checkout','OrderController@saveCheckout');
// Route::get('checkout','OrderController@checkout');
// Route::get('checkout_payment','OrderController@checkoutPayment');
// Route::get('payment-done','OrderController@paymentDone');
// Route::post('order/save-order','OrderController@saveOrder');
// Route::get('order/cartRemove/{cart_id}','OrderController@cartRemove');
// Route::post('stripe', 'OrderController@stripePost')->name('stripe.post');

// //Ajax Controller
// Route::get('search','AjaxController@searchResult');
// Route::get('search-blog','AjaxController@searchBlogResult');
Route::get('client-log','AjaxController@clienLog');
// Route::get('getCountry','AjaxController@country');
// Route::get('getProvince','AjaxController@province');

