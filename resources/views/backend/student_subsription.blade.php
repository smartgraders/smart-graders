<!-- First you need to extend the CB layout -->
@extends('crudbooster::admin_template')
@section('content')

<div class='panel panel-default'>
    <h3 class='panel-heading'>Subscription's List </h3>
    <div class='panel-body'>
        
        {{ Session::get('message') }}
        
        <!-- Your custom  HTML goes here -->
        <table class='table table-striped table-bordered'>
            <thead>
                <tr class="active">
                    <th width="auto">Subject</th>
                    <th width="auto">Status</th>
                    <th width="auto">Cost</th>
                    <th width="auto">Discount</th>
                    <th width="auto">Total</th>
                    <!--<th width="auto">Action</th>-->
                </tr>
            </thead>
            <tbody>
                @php Session::put('message', ''); $sum=0; @endphp
                @foreach($subjects as $row)
                @php if( (float)$row->total>0 && $row->status==1 ) $sum+= $row->total; @endphp
                <tr>
                    <td>{{ $row->title }}</td>
                    <td>
                    @if($row->status==0)
                        <span class='label label-warning'>Inactive</span>
                        @elseif($row->status==1) 
                        <span class='label label-success'>Subscribed</span
                        @elseif($row->status==2) 
                        <span class='label label-warning'>Paused</span> 
                        @elseif($row->status==3) 
                        <span class='label label-info'>Requested</span>
                        @endif
                    
                    </td>
                    <td>{{ $row->price ? "$".number_format($row->price) : "$0.00" }}</td>
                    <td>{{ $row->discount ? $row->discount."%" : "0%" }}</td>
                    <td>{{ $row->total ? "$".number_format($row->total) : "$0.00" }}</td>
                    <!--<td>-->
                         
                    <!--    @if($row->status==0)-->
                    <!--    <a title='Click here to pause' class='btn btn-xs btn-warning' href='{{ CRUDBooster::adminPath("cms-student/pause/$row->sub_id") }}'><i class='fa fa-pause'></i> Pause</a> <a title='Click here to unsubscribe' class='btn btn-xs btn-danger' href='{{ CRUDBooster::adminPath("cms-student/stop/$row->sub_id") }}'><i class='fa fa-stop'></i> Unsubscribe</a>-->
                    <!--    @elseif($row->status==1) -->
                    <!--    <a title='Click here to pause' class='btn btn-xs btn-warning' href='{{ CRUDBooster::adminPath("cms-student/pause/$row->sub_id") }}'><i class='fa fa-pause'></i> Pause</a> <a title='Click here to unsubscribe' class='btn btn-xs btn-danger' href='{{ CRUDBooster::adminPath("cms-student/stop/$row->sub_id") }}'><i class='fa fa-stop'></i> Unsubscribe</a>-->
                    <!--    @elseif($row->status==2) -->
                    <!--    <a title='Click here to Start' class='btn btn-xs btn-success' href='{{ CRUDBooster::adminPath("cms-student/start/$row->sub_id") }}'><i class='fa fa-play'></i> Start</a> <a title='Click here to unsubscribe' class='btn btn-xs btn-danger' href='{{ CRUDBooster::adminPath("cms-student/stop/$row->sub_id") }}'><i class='fa fa-stop'></i> Unsubscribe</a>-->
                    <!--    @elseif($row->status==3) -->
                    <!--    <a title='Click here to Start' class='btn btn-xs btn-success' href='{{ CRUDBooster::adminPath("cms-student/start/$row->sub_id") }}'><i class='fa fa-play'></i> Subscribe</a>-->
                    <!--    @endif-->
                        
                    

                    <!--</td>-->
                </tr>
                
                
                @endforeach
                
                <tr>
                    <td colspan="4" style="text-align:right"><h3>Total</h3></td>
                    <td colspan="2"><h3>{{ "$".number_format($sum) }}</h3></td>
                </tr>
            </tbody>
        </table>
        
    </div>
    
</div>
@endsection