<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
  <!-- Your html goes here -->
  <div class='panel panel-default'>
    <h3 class='panel-heading'>{{ $row->first_name.' '.$row->last_name }} Payment History</h3>
    <div class='panel-body'> 
      <div class='table-responsive'>
        
        <table id='table-detail' class='table table-striped'>
          <tbody><tr>
            <td>
              <h3><label>Beginner Driver Education Program</label></h3>
              <h4 class="regular">Regular Fees: <span>${{ $programme->regular_fee }} + HST</span></h4>
              <h4 class="prom">Promotion Fees: <span>Save ${{ $programme->promotion_fee }}</span></h4>
              <h4 class="pay_only">Pay Only Fees: <span>${{ $programme->fee }} + HST</span></h4>
            </td>
            <td>
              @if ($programme_addon)
              <h3><label>One Time Offer Savings,</label></h3>
              @foreach ($programme_addon as $item)
              <h4>{{ $item->title }} - CAD ${{ number_format($item->amount,2) }}</h4>
              @endforeach
              @endif
            </td>
          </tr>
          <tr style="background: #f5f5f5">
            <td>
              <div class="total">
                <h4>Subtotal :$<span class="_subtotal">{{ number_format($sum,2) }}</span></h4>
                <h4>HST (13%) : $<span class="_hst">{{ number_format($tax,2) }}</span></h4>
                <h3>Total Fees : $<span class="_total">{{ number_format($amount,2) }}</span></h3>
              </div>
            </td>
            <td>
              @if ($row->payment_status==1)
              <button type="button" class="btn btn-lg btn-success" style="font-size: 60px;">Paid</button>
              @else                  
              <button type="button" class="btn btn-lg btn-danger" style="font-size: 60px;">Amount Due</button>
              @endif
            </td>
          </tr>
          </tbody></table>
      </div>         
        <div class='form-group'>
          <p>@php
              //echo '<pre>'; print_r($row)
          @endphp </p>
        </div>
    </div>
  </div>
@endsection