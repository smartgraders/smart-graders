<!-- First you need to extend the CB layout -->
@extends('crudbooster::admin_template')
@section('content')

<div class='panel panel-default'>
    <h3 class='panel-heading'>Schedule List </h3>
    <div class='panel-body'>
        <!-- Your custom  HTML goes here -->
        <table class='table table-striped table-bordered'>
            <thead>
                <tr class="active">
                    <th width="auto">Batch</th>
                    <th width="auto">Subject</th>
                    <th width="auto">Teacher</th>
                    <th width="auto">Start Date</th>
                    <th width="auto">Start Time</th>
                    <th width="auto">Days</th>
                </tr>
            </thead>
            <tbody>
                @foreach($batches as $row)
                <tr>
                    <td>{{ $row->batch_name }}</td>
                    <td>{{ $row->title }}</td>
                    <td>{{ $row->name }}</td>
                    <td>{{ $row->batch_start_date }}</td>
                    <td>{{ $row->batch_start_time }}</td>
                    <td>{{ str_replace(";",", ",$row->batch_days) }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        
    </div>
    
</div>
@endsection