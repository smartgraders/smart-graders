<!-- First you need to extend the CB layout -->
@extends('crudbooster::admin_template')
@section('content')
<p><a title="Return" href="http://localhost/g1g2/admin/cms_users"><i class="fa fa-chevron-circle-left "></i>
    &nbsp; Back To List Data Instructor</a></p>
<div class='panel panel-default'>
    <h3 class='panel-heading'>{{ $instructor->name }}'s Invoices</h3>
    <div class='panel-body'>
        <!-- Your custom  HTML goes here -->
        <table class='table table-striped table-bordered'>
        <thead>
            <tr>
                <th>Student Name</th>
                <th>Lessons</th>
                <th>Total Charges</th>
                <th>Status</th>
                <th>Created Date</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($invoices as $row)
            <tr>
                <td>{{ $row->first_name.' '.$row->last_name }}</td>
                <td>{{ str_replace("1000","Extra",$row->lessions) }} lesson</td>
                <td>{{ "CAD $".number_format($row->total_charges,2) }} </td>
                <td>{!! $row->status=="Paid" ? "<span class='label label-success'>Paid</span>":"<span class='label label-warning'>Progress</span>" !!}</td>
                <td>{{ $row->created_at }} </td>
                <td>
                <!-- To make sure we have read access, wee need to validate the privilege -->
                @if(CRUDBooster::isUpdate() && $button_edit)
                <a class='btn btn-xs btn-success btn-edit' href='{{CRUDBooster::mainpath("edit/$row->id")}}'><i class="fa fa-pencil"></i></a>
                @endif
                
                @if(CRUDBooster::isDelete() && $button_edit)
                <a class='btn btn-xs btn-warning btn-delete' title='Delete' href='javascript:;'
           onclick='swal({   
				title: "Are you sure ?",   
				text: "You will not be able to recover this record data!",   
				type: "warning",   
				showCancelButton: true,   
				confirmButtonColor: "#ff0000",   
				confirmButtonText: "Yes!",  
				cancelButtonText: "No",  
				closeOnConfirm: false }, 
				function(){  location.href="{{ CRUDBooster::mainpath("delete/$row->id") }}" });'><i class="fa fa-trash"></i></a>
                @endif
                </td>
            </tr>
            @endforeach
        </tbody>
        </table>

        <!-- ADD A PAGINATION -->
        <p>{!! urldecode(str_replace("/?","?",$invoices->appends(Request::all())->render())) !!}</p>
    </div>
    
</div>
@endsection