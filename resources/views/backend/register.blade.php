<!-- First you need to extend the CB layout -->
@extends('crudbooster::admin_template')
@section('content')
<p><a title="Return" href="{{url('admin/student')}}/"><i class="fa fa-chevron-circle-left "></i>
    &nbsp; Back To List Data Instructor</a></p>
<div class='panel panel-default'>
    <h3 class='panel-heading'>Register</h3>
    <div class='panel-body'>
        <form action="" method="POST">
            {!! csrf_field() !!}
            <div class='panel-body'> 
                @php
                if(!empty(Session::get('message'))) { echo Session::get('message'); Session::put('message', ''); }
                @endphp

                <div class="form-group header-group-0 clearfix" id="form-group-followed_up" style="">
                    <label class="control-label col-sm-1">Type <span class="text-danger" title="This field is required">*</span></label>
                    <div class="col-sm-4">        
                        <div class=" ">
                            <label class="radio-inline">
                                <input type="radio" name="type" value="Student" checked> Student
                            </label>
                        
                            <label class="radio-inline">
                                <input type="radio" name="type" value="Teacher"> Teacher
                            </label>
                        </div>                            
                        <div class="text-danger"></div>
                        <p class="help-block"></p>
                    </div>
                </div>

                <div class="form-group header-group-0 clearfix" id="form-group-email" style="">
                    <label class="control-label col-sm-1">Email
                                    <span class="text-danger" title="This field is required">*</span>
                            </label>
                
                    <div class="col-sm-4">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            <input type="email" name="email" style="display: none">
                            <input type="email" title="Email" required="" placeholder="Please enter a valid email address" maxlength="255" class="form-control" name="email" id="email" value="">
                        </div>
                        <div class="text-danger"></div>
                        <p class="help-block"></p>
                    </div>
                </div>

                <div class="form-group header-group-0 clearfix" id="form-group-last_name" style="">
                    <label class="control-label col-sm-1">
                        Code
                                    <span class="text-danger" title="This field is required">*</span>
                            </label>
                
                    <div class="col-sm-4">
                        <input type="text" title="Code" required="" maxlength="255" class="form-control" name="code" id="code" value="{{ substr(strtoupper(md5(rand(11,99).rand(11,99).rand(11,99).rand(11,99).rand(11,99))),12) }}">
                
                        <div class="text-danger"></div>
                        <p class="help-block"></p>
                
                    </div>
                </div>

                
            </div>
            <div class="panel-footer">
                <input type="submit" name="submit" class="btn btn-success" value="Send Registration">
            </div>
            </form>
    </div>
    
</div>
@endsection
