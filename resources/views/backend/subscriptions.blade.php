<!-- First you need to extend the CB layout -->
@extends('crudbooster::admin_template')
@section('content')
<p><a title="Return" href="http://smartgraders.com/admin/"><i class="fa fa-chevron-circle-left "></i>
    &nbsp; Back To Dashboard</a></p>
<div class='panel panel-default'>
    <h3 class='panel-heading'>Subscription's List <a title="Add Student Subscription" target="_blank" class='btn btn-xs btn-success btn-add' href='{{CRUDBooster::adminPath("cms-subscriptions/add")}}'><i class="fa fa-plus"></i> Add Student Subscription</a></h3>
    
    
    <div class="box-header">
    <div class="box-tools pull-right" style="position: relative;">

                 <a href="http://smartgraders.com/ashu/admin/cms-subscriptions/list" id="btn_advanced_filter" data-url-parameter="" title="Clear" class="btn btn-sm btn-default ">
                        <i class="fa fa-sync"></i> Clear
                    </a>                   
                
                <form method="get" style="display:inline-block;width: 200px;" action="http://smartgraders.com/ashu/admin/cms-subscriptions/list">
                    <div class="input-group">
                        <input type="text" name="name" value="{{$_GET['name']}}" class="form-control input-sm pull-right" placeholder="Search">
                        
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </form>


                <form method="get" id="form-limit-paging" style="display:inline-block" action="http://smartgraders.com/ashu/admin/cms-subscriptions/list">
                    
                    <div class="input-group">
                        <select onchange="$('#form-limit-paging').submit()" name="limit" style="width: 56px;" class="form-control input-sm">
                            <option @if(!empty($_GET['name']) && $_GET['name']==5) selected @endif value="5">5</option>
                            <option @if(!empty($_GET['name']) && $_GET['name']==10) selected @endif value="10">10</option>
                            <option @if(!empty($_GET['name']) && $_GET['name']==20) selected @endif value="20">20</option>
                            <option @if(!empty($_GET['name']) && $_GET['name']==25) selected @endif value="25">25</option>
                            <option @if(!empty($_GET['name']) && $_GET['name']==50) selected @endif value="50">50</option>
                            <option @if(!empty($_GET['name']) && $_GET['name']==100) selected @endif value="100">100</option>
                            <option @if(!empty($_GET['name']) && $_GET['name']==200) selected @endif value="200">200</option>
                        </select>
                    </div>
                </form>

            </div>
    
    </div>
    
    
    
    
    <div class='panel-body'>
        <!-- Your custom  HTML goes here -->
        <table class='table table-striped table-bordered'>
            <!--<thead>-->
            <!--    <tr class="active">-->
            <!--        <th width="auto">Student Name</th>-->
            <!--        <th width="auto">Grade</th>-->
            <!--        <th width="auto">Subjets</th>-->
            <!--        <th width="auto">Cost</th>-->
            <!--        <th width="auto">Discount</th>-->
            <!--        <th width="auto">Total</th>-->
            <!--        <th width="auto">Status</th>-->
            <!--        <th width="auto" style="text-align:right">Action</th>-->
            <!--    </tr>-->
            <!--</thead>-->
            <tbody>
                @php 
                    $studid = 0; $sum=0; $i=0; $count=count($subscribers);
                @endphp
                @foreach($subscribers as $row)
                
                @php if($studid!=$row->student_id || $i==$count) { $studid = $row->student_id;  @endphp
                @php 
                
                if($i==0) $sum = $sum + $row->total;
                if($i!=0) { @endphp
                <tr style="background:#ddd">
                    <th width="auto" colspan='3' style="text-align:right">
                        <h3>Total</h3>
                    </th>
                    <th width="auto">
                        <h3>${{$sum}}</h3>
                    </th>
                    <th width="auto" colspan='2'>
                        <!--<h3><a title="Add Student Subscription" target="_blank" class='btn btn-xs btn-success btn-add' href='{{CRUDBooster::adminPath("cms-subscriptions/subscribe/$row->student_id")}}'><i class="fa fa-plus"></i> Add Subscription</a></h3>-->
                    </th>
                </tr>
                @php  $sum=0; $sum = $sum + $row->total; } @endphp
                <tr style="background:#d4edda">
                    <th width="auto"><h3>Student Name</h3></th><th colspan="2"><h3>{{ $row->name }}</h3></th><th width="auto"><h3>Grade</h3></th><th><h3>{{ $row->title }}</h3></th>
                    <td><h3><a title="Add Student Subscription" target="_blank" class='btn btn-xs btn-success btn-add' href='{{CRUDBooster::adminPath("cms-subscriptions/subscribe/$row->student_id")}}'><i class="fa fa-plus"></i> Add Subscription</a></h3></td>
                </tr>
                <tr class="active">
                    <th width="auto">Subjets</th>
                    <th width="auto">Cost</th>
                    <th width="auto">Discount</th>
                    <th width="auto">Total</th>
                    <th width="auto">Status</th>
                    <th width="auto">Action</th>
                </tr>
                @php } else { $sum = $sum + $row->total; } $i++; @endphp
                
                <tr>
                    <td>{{ $row->subject }}</td>
                    <td>${{ (float)$row->cost }}</td>
                    <td>{{ (int)$row->discount }}%</td>
                    <td>${{ (float)$row->total }}</td>
                    <td>
                        {{ $row->status==1?'Active':'Inactive' }}
                    </td>
                    
                    <!--<td>-->
                    <!--    <select name="subjects[]" >-->
                    <!--    <option value="0">Select subject</option>-->
                    <!--    @foreach($subjects as $sub)-->
                    <!--    <option @if($sub->id==$row->subject_id) selected @endif value="{{$sub->id}}">{{$sub->title}}</option>-->
                    <!--    @endforeach-->
                    <!--    </select>-->
                    <!--</td>-->
                    <!--<td><input type="text" name="cost[]" value="${{ (float)$row->cost }}" /></td>-->
                    <!--<td><input type="text" name="cost[]" value="{{ (int)$row->discount }}%" /></td>-->
                    <!--<td><input type="text" name="cost[]" value="${{ (float)$row->total }}" /></td>-->
                    <!--<td>-->
                    <!--    <select name="status[]">-->
                    <!--        <option {{ $row->status==1?'selected':'' }} value="1">Active</option>-->
                    <!--        <option {{ $row->status==0?'selected':'' }} value="0">Inactive</option>-->
                    <!--    </select>-->
                    <!--</td>-->
                    <td>
                    <!-- To make sure we have read access, wee need to validate the privilege -->
                    @if(CRUDBooster::isUpdate() && $button_edit)
                    <a title="Edit Subject" class='btn btn-xs btn-success btn-edit' href='{{CRUDBooster::adminPath("cms-subscriptions/edit/$row->id")}}'><i class="fa fa-pencil"></i></a>
                    <!--<a title="Change assign instructor" class='btn btn-xs btn-success btn-edit' href='{{CRUDBooster::adminPath("cms-subscriptions/assign/$row->id")}}'><i class="fa fa-exchange"></i></a>-->
                    @endif
                    
                    @if(CRUDBooster::isDelete() && $button_edit)
                    <a class='btn btn-xs btn-warning btn-delete' title='Delete' href='javascript:;' onclick='swal({   
    				title: "Are you sure ?",   
    				text: "You will not be able to recover this record data!",   
    				type: "warning",   
    				showCancelButton: true,   
    				confirmButtonColor: "#ff0000",   
    				confirmButtonText: "Yes!",  
    				cancelButtonText: "No",  
    				closeOnConfirm: false }, 
    				function(){  location.href="{{ CRUDBooster::adminPath("cms-subscriptions/delete/$row->id") }}" });'><i class="fa fa-trash"></i></a>
                    @endif
                    
                    <!--<a title="Add Subject" class='btn btn-xs btn-success btn-add' href='{{CRUDBooster::adminPath("cms-subscriptions/add")}}'><i class="fa fa-plus"></i></a>-->
                    
                    </td>
                </tr>
                
                @php if($i==$count) { $studid = $row->student_id;  @endphp
                <tr style="background:#ddd">
                    <th width="auto" colspan='3' style="text-align:right">
                        <h3>Total</h3>
                    </th>
                    <th width="auto">
                        <h3>${{$sum}}</h3>
                    </th>
                    <th width="auto" colspan='2'>
                        <!--<h3><a title="Add Student Subscription" target="_blank" class='btn btn-xs btn-success btn-add' href='{{CRUDBooster::adminPath("cms-subscriptions/subscribe/$row->student_id")}}'><i class="fa fa-plus"></i> Add Subscription</a></h3>-->
                    </th>
                </tr>
                @php } @endphp
                
                @endforeach
            </tbody>
        </table>
        
    </div>
    
</div>
@endsection