<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
  <!-- Your html goes here -->
  <div class='panel panel-default'>
    <h3 class='panel-heading'>{{ $row->first_name.' '.$row->last_name }}</h3>

    {{-- <form action="{{CRUDBooster::mainpath('edit-save/'.$row->id)}}" method="POST"> --}}
    <form action="" method="POST">
    {!! csrf_field() !!}
    <div class='panel-body'>
      @php
      $lessons_list = is_array($lessons_list) ? array_unique($lessons_list) : [];	
      $list = is_array($list) ? $list : [];	
      if(!empty(Session::get('message'))) { echo Session::get('message'); Session::put('message', ''); }
      @endphp
 
      <div class='form-group'><h3 class="title">Select lesson for invoice</h3></div>
      <div class='form-group1'>          
        @for ($i=1; $i<=10; $i++)
        <label class="@php echo !in_array($i, $list) ? 'disabled' : 'enabled'; @endphp">
          <input name="lession[]" type="checkbox" value="{{ $i }}"  
          {{ ( !in_array($i, $list) || in_array($i, $lessons_list)) ? 'disabled' : '' }} 
          {{ ( in_array($i, $lessons_list) ) ? 'checked' : '' }}> Lesson {{ $i }}</label>
        @endfor
        <label class="@php echo !in_array(1000, $list) ? 'disabled' : 'enabled'; @endphp"><input name="lession[]" type="checkbox" value="1000" @php echo !in_array(1000, $list) ? 'disabled' : ''; @endphp {{ (isset($lesson) && ($lesson->lession==1000) ) ? 'checked' : '' }}> Extra Lesson</label>
      </div>        

    </div>
    <div class="panel-footer">
      <input type="submit" name="submit" class="btn btn-md btn-success" value="Submit">
    </div>

    </form>
</div>

@endsection