<!-- First you need to extend the CB layout -->
@extends('crudbooster::admin_template')
@section('content')
<p><a title="Return" href="http://localhost/g1g2/admin/cms_users"><i class="fa fa-chevron-circle-left "></i>
    &nbsp; Back To List Data Instructor</a></p>
<div class='panel panel-default'>
    <h3 class='panel-heading'>{{ $instructor->name }}'s List</h3>
    <div class='panel-body'>
        <!-- Your custom  HTML goes here -->
        <table class='table table-striped table-bordered'>
        <thead>
            <tr>
                <th>Name</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($rows as $row)
            <tr>
                <td>
                    <h3 class="title">{{ $row->first_name.' '.$row->last_name }}</h3>
                    <p><i class="fa fa-envelope-o"></i> {{ $row->email }}</p>
                    <p><i class="fa fa-phone"></i> {{ $row->phone }}</p>
                    <p><i class="fa fa-calendar"></i> {{ $row->prefered_start_date }}</p>
                </td>
                <td>
                <!-- To make sure we have read access, wee need to validate the privilege -->
                @if(CRUDBooster::isUpdate() && $button_edit)
                <a title="Edit Student" class='btn btn-xs btn-success btn-edit' href='{{CRUDBooster::adminPath("student/edit/$row->id")}}'><i class="fa fa-pencil"></i></a>
                <a title="Change assign instructor" class='btn btn-xs btn-success btn-edit' href='{{CRUDBooster::adminPath("student/assign/$row->id")}}'><i class="fa fa-exchange"></i></a>
                @endif
                
                @if(CRUDBooster::isDelete() && $button_edit)
                <a class='btn btn-xs btn-warning btn-delete' title='Delete' href='javascript:;'
           onclick='swal({   
				title: "Are you sure ?",   
				text: "You will not be able to recover this record data!",   
				type: "warning",   
				showCancelButton: true,   
				confirmButtonColor: "#ff0000",   
				confirmButtonText: "Yes!",  
				cancelButtonText: "No",  
				closeOnConfirm: false }, 
				function(){  location.href="{{ CRUDBooster::adminPath("student/delete/$row->id") }}" });'><i class="fa fa-trash"></i></a>
                @endif
                </td>
            </tr>
            @endforeach
        </tbody>
        </table>

        <!-- ADD A PAGINATION -->
        {{-- <p>{!! urldecode(str_replace("/?","?",$rows->appends(Request::all())->render())) !!}</p> --}}
    </div>
    
</div>
@endsection