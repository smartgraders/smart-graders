<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
  <!-- Your html goes here -->
  <div class='panel panel-default'>
    <h3 class='panel-heading'>{{ $row->first_name.' '.$row->last_name }}</h3>

    {{-- <form action="{{CRUDBooster::mainpath('edit-save/'.$row->id)}}" method="POST"> --}}
    <form action="" method="POST">
    {!! csrf_field() !!}
    <div class='panel-body'>
      @php
      if(!empty(Session::get('message'))) { echo Session::get('message'); Session::put('message', ''); }
      @endphp
 
        <div class='form-group'><h3 class="title"><span>Start Date:</span> {{ $row->prefered_start_date }}</h3></div>
        <div class='form-group'>
          
            <label for="">Select Lesson</label> 
            <select name="lession" id="lession" class="form-control" @if (!empty(Session::get('student_lessons_id'))) disabled @endif>
              <option value="">Select Lesson for {{ $row->first_name.' '.$row->last_name }}</option>
              @for ($i=1; $i<=10; $i++)
                <option value="{{ $i }}"  @php echo is_array($list) && in_array($i, $list) ? 'disabled' : ''; @endphp {{ (isset($lesson) && ($lesson->lession==$i) ) ? 'selected' : '' }}> Lession {{ $i }}</option> 
              @endfor
              <option value="1000" {{ (isset($lesson) && ($lesson->lession==1000) ) ? 'selected' : '' }}> Extra Lesson</option>
            </select>
        </div>

        @if (!empty(Session::get('student_lessons_id')))
        <input type="hidden" id="num" name="total_time" class="form-control" min="0">
        <div id="timer" class="col-12">
          <div class="clock-wrapper">
              <span class="hours">00</span>
              <span class="dots">:</span>
              <span class="minutes">00</span>
              <span class="dots">:</span>
              <span class="seconds">00</span>
          </div>
        </div>
        @endif

    </div>
    <div class="panel-footer">
      {{-- <input type="submit" name="end" class="btn btn-md btn-success" disabled value="End Lession"> --}}
      @if (!empty(Session::get('student_lessons_id')))
      <button type="button" class="btn btn-md btn-danger" id="stop-timer">Stop Timer</button>
      <button type="submit" class="btn btn-md btn-success" id="submit-timer" name="end">Submit</button>
      <button type="button" class="btn btn-md btn-warning" id="resume-timer">Resume Timer</button>  
      @else
      <input type="submit" name="start" class="btn btn-md btn-success" value="Start Lesson">  
      @endif
      </div>

    </div>



    </form>
</div>

@endsection