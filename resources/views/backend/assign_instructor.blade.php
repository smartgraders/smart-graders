<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
  <!-- Your html goes here -->
  <div class='panel panel-default'>
    <h3 class='panel-heading'>{{ $row->first_name.' '.$row->last_name }}</h3>
    {{-- <form action="{{CRUDBooster::mainpath('edit-save/'.$row->id)}}" method="POST"> --}}
    <form action="" method="POST">
    {!! csrf_field() !!}
    <div class='panel-body'> 
        @php
        if(!empty(Session::get('message'))) { echo Session::get('message'); Session::put('message', ''); }
        @endphp
        <div class='form-group'><span>Start Date:</span> {{ $row->prefered_start_date }}</div>
        <div class='form-group'>
            <label for="">Select Instructor</label> 
            <select name="assigned_instructor_id" id="assigned_instructor_id" class="form-control">
              <option value="">Select assigned instructor</option>
              @foreach ($instructor as $item)
                <option value="{{ $item->id }}" {{ $row->assigned_instructor_id==$item->id ? 'selected' : '' }}>{{ $item->name }}</option> 
              @endforeach
            </select>
        </div>
    </div>
    <div class="panel-footer">
        <input type="submit" name="submit" class="btn btn-success" value="Submit">
    </div>
    </form>
</div>
@endsection