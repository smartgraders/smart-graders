@extends('frontend.layout.app')
@section('content')

<!-- ======= Hero Section ======= -->
<section id="breadcrumbs" class="breadcrumbs">
    <div class="container">
      <div class=" justify-content-between align-items-center">
        <h2 style="color: #4251b5; font-weight: bold; margin-top: 20px; text-align:center">Teacher Registration</h2>
      </div>
      <form class="form-horizontal" action="" style="background: #fff; border-radius: 20px; padding: 30px; margin:30px auto; width:80%" method="post" name="userprofileFormOne" id="userprofileFormOne">

        {{ csrf_field() }}
        <input type="hidden" class="form-control" id="code"  name="code" value="{{ $_GET['token'] }}">
        <ul class="alert alert-danger" style="display:none"></ul>

        <div class="form-group">
          <label class="control-label col-sm-12" for="email">Teacher Name:</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="name"  name="name">
          </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-12" for="email">Email:</label>
            <div class="col-sm-10">
              <input type="email" class="form-control" id="email"  name="email">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-12" for="phone">Phone:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="phone"  name="phone">
            </div>
          </div>
          <div class="form-group">
          <label class="control-label col-sm-12 " for="teacher_grade">Grade</label>
          <div class="col-sm-10">
            <select name="teacher_grade[]" id="teacher_grade" class="form-control" multiple>
              <option value="0">Select Grade</option>
                @foreach($classes as $class)
                <option value="{{$class->id}}">{{$class->title}}</option>
                @endforeach
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-12" for="teacher_subject">Subjects</label>
          <div class="col-sm-10">
            <select name="teacher_subject[]" id="teacher_subject" class="form-control" multiple>
              <option value="0">Select subjects</option>
                @foreach($subjects as $subject)
                <option value="{{$subject->id}}">{{$subject->title}}</option>
                @endforeach
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-12" for="wage_type">Wage</label>
          <div class="row form-group" style="margin:0 0">
          <div class="col-sm-4">
            <select name="wage_type" id="wage_type" class="form-control">
              <option value="Fixed">Fixed</option><option value="Hourly">Hourly</option>
            </select>
          </div>
          <div class="col-sm-6">
            <input type="text" class="form-control" id="wage_charge"  name="wage_charge">
          </div>
          </div>
        </div>

        <!--<div class="form-group">-->
        <!--    <label class="control-label col-sm-12" for="email">Username:</label>-->
        <!--    <div class="col-sm-10">-->
        <!--      <input type="text" class="form-control" id="username"  name="username">-->
        <!--    </div>-->
        <!--  </div>-->

          <div class="form-group">
            <label class="control-label col-sm-12" for="password">Password:</label>
            <div class="col-sm-10">
              <input type="password" class="form-control" id="password"  name="password">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-sm-12" for="confirm_password">Confirm Password:</label>
            <div class="col-sm-10">
              <input type="password" class="form-control" id="confirm_password"  name="confirm_password">
            </div>
          </div>
        
        
        <div class="form-group">
          <label class="control-label col-sm-12" for="email">Note</label>
          <div class="col-sm-10">
            <textarea cols="100" rows="5" class="form-control"></textarea>
          </div>
        </div>
        <div class="form-group" style="margin-top: 30px; margin-bottom: 60px;">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="button" class="btn btn-primary subs" id="register-now">Register Now</button>
            <button type="reset" class="btn btn-secondary">Cancel</button>
          </div>
        </div>
      </form>
    </div>
  </section>
  <!-- End Hero -->
  <main id="main">
    <!-- ======= Featured Services Section ======= -->
  </main>
  <!-- End #main -->
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <script>
  
  jQuery(document).ready(function() {
    jQuery('#teacher_grade').select2();
    jQuery('#teacher_subject').select2();

jQuery('#register-now').click(function(){
    var the = $(this);
      the.text('Please wait...');
    jQuery('.alert-danger').hide();
    var data = jQuery('#userprofileFormOne').serialize();    
    jQuery.ajax({
      url: "{{ url('/teacher_reg_form') }}",
      method: 'post',
      data: data,
      success: function(data){
          the.text('Register Now');
        jQuery('.alert-danger').show().html('');
        if(data.type=='error') {
          jQuery.each(data.errors, function(key, value){
            jQuery('.alert-danger').append('<li>'+value+'</li>');
          });
          jQuery('html, body').animate({ scrollTop: (jQuery('.alert-danger').offset().top)-50 }, 1000); 
        }
        else {
          jQuery('#userprofileFormOne')[0].reset();
          alert(data.msg);
          jQuery('.alert-danger').hide();
        }
        console.log(data);
        //window.location.href = "{{ url('program-and-fees/'.$slug.'/final') }}";
      }
    });
  });
  });
  </script>
  @endsection