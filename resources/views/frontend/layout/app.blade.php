@php
use App\Page;
use App\Locations;
use App\Helpers\Helper;

if(Request::segment(1)=='') $slug = 'home'; else $slug = Request::segment(1);

if( Request::segment(1)=='city' && !empty(Request::segment(2))) {
	$city_slug = Request::segment(2);
	$slug = 'home';
}
else if( Request::segment(1)=='page' && !empty(Request::segment(2))) {
	$slug = Request::segment(2);
	$city_slug = empty(Session::get('city_slug')) ? Helper::cookie() : Session::get('city_slug') ;
}
else {
	$city_slug = empty(Session::get('city_slug')) ? Helper::cookie() : Session::get('city_slug') ;
}

//if(Request::segment(3)) $slug = Request::segment(3);

$location_list = Locations::Where('status',1)->orderBy('title','ASC')->get();

@endphp
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="_token" content="{{csrf_token()}}" />
	

	@php
	//echo Request::segment(1).'---'.Request::segment(2).'---'.Request::segment(3);
	//$page = Page::Where('slug', $slug)->orWhere('slug', Request::segment(2))->orWhere('slug', Request::segment(3))->first();
	$page = Page::Where('slug', $slug)->first();
	$title = $page->meta_title;
	$title = str_replace('{{LOCATION}}',$city_slug, $title);
    $title = ucwords(str_replace('-', ' ', $title));
    $keyword = $page->meta_description;
    $description = $page->meta_keyword;
    @endphp
    <title>{!! $title !!} </title>
    <meta name="keywords" content="{!! $keyword !!}">
    <meta name="description" content="{!! $description !!}">

	<link href="assets/img/favicon.png" rel="icon">
	<link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!-- Vendor CSS Files -->

	{!! Html::style('resources/assets/vendor/bootstrap/css/bootstrap.min.css') !!}
	{!! Html::style('resources/assets/vendor/icofont/icofont.min.css') !!}
	{!! Html::style('resources/assets/vendor/boxicons/css/boxicons.min.css') !!}
	{!! Html::style('resources/assets/vendor/venobox/venobox.css') !!}
	{!! Html::style('resources/assets/vendor/owl.carousel/assets/owl.carousel.min.css') !!}
	{!! Html::style('https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons') !!}


	<!-- Template Main CSS File -->
	{!! Html::style('resources/assets/css/style.css') !!}
	<!-- =======================================================
	* Template Name: eNno - v2.1.0
	* Template URL: https://bootstrapmade.com/enno-free-simple-bootstrap-template/
	* Author: BootstrapMade.com
	* License: https://bootstrapmade.com/license/
  	======================================================== -->
<style>
.accordion {
    background-color: #f2f0ce;
    color: #000;
    cursor: pointer;
    padding: 18px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 18px;
    font-weight: bold;
    transition: 0.4s;
    margin-bottom: 5px;
}
.active, .accordion:hover {
    background-color: #fff;
}
.panel {
    padding: 0 18px;
    display: none;
    background-color: white;
    overflow: hidden;
    margin-top: -5px;
    margin-bottom: 5px;
}
button.accordion.active:after {
    content: "\2212";
}
button.accordion:after {
    content: '\002B';
    color: #777;
    font-weight: bold;
    float: right;
    margin-left: 5px;
}
.days a{
    border: solid 1px #ccc;
    border-radius: 5px;
    padding: 5px 10px;
	 display:inline-block; margin-right: 10px;
}
	
	.material-icons{color:#fff;}
</style>
{!! Html::script('resources/assets/vendor/jquery/jquery.min.js') !!}
	
</head>
<body>
<!-- ======= Header ======= -->
<header id="header" class="fixed-top">
	<div class="container d-flex align-items-center">
	  <h1 class="logo mr-auto">{!! Html::image('resources/assets/images/logo.png') !!}</h1>
	  <!-- Uncomment below if you prefer to use an image logo -->
	  <!-- <a href="index.html" class="logo mr-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
	  <nav class="nav-menu d-none d-lg-block">
		<ul>
		  <li><a href="index.html">Home</a></li>
		  <li><a href="courses.html">Courses </a></li>
		  <li><a href="index.html#contact"> Contact Us </a></li>
		  <li class="active"><a href="Enroll.html">Enroll Now </a></li>
		</ul>
	  </nav>
	  <!-- .nav-menu -->
	</div>
  </header>
  <!-- End Header -->


    <!--************************************
				Body Part Start
		*************************************-->

        @yield('content')
        
    <!--************************************
				Body Part End
        *************************************-->
        

        <!-- ======= Footer ======= -->
<footer id="footer">
	<div class="container footer-bottom clearfix">
	  <div class="copyright"> &copy; Copyright <strong><span>Smart Graders</span></strong>. All Rights Reserved </div>
	  <div class="credits">
		<!-- All the links in the footer should remain intact. -->
		<!-- You can delete the links only if you purchased the pro version. -->
		<!-- Licensing information: https://bootstrapmade.com/license/ -->
		<!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/enno-free-simple-bootstrap-template/ -->
		<div class="social-links"> <a href="https://twitter.com/Smartgraders2" target="-blank"  class="twitter"><i class="bx bxl-twitter"></i></a> <a href="https://www.facebook.com/Smartgraders" target="-blank"  class="facebook"><i class="bx bxl-facebook"></i></a> <a href="https://www.linkedin.com/in/smartgraders/" class="linkedin" target="-blank" ><i class="bx bxl-linkedin"></i></a> </div>
	  </div>
	</div>
  </footer>
  <!-- End Footer -->
  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
  <!-- Vendor JS Files -->

  
  {!! Html::script('resources/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') !!}
  {!! Html::script('resources/assets/vendor/jquery.easing/jquery.easing.min.js') !!}
  {!! Html::script('resources/assets/vendor/php-email-form/validate.js') !!}
  {!! Html::script('resources/assets/vendor/waypoints/jquery.waypoints.min.js') !!}
  {!! Html::script('resources/assets/vendor/counterup/counterup.min.js') !!}
  {!! Html::script('resources/assets/vendor/isotope-layout/isotope.pkgd.min.js') !!}
  {!! Html::script('resources/assets/vendor/venobox/venobox.min.js') !!}
  {!! Html::script('resources/assets/vendor/owl.carousel/owl.carousel.min.js') !!}
  {!! Html::script('resources/assets/js/main.js') !!}
  <!-- Template Main JS File -->
  <script>
  var acc = document.getElementsByClassName("accordion");
  var i;
  
  for (i = 0; i < acc.length; i++) {
	acc[i].addEventListener("click", function() {
	  this.classList.toggle("active");
	  var panel = this.nextElementSibling;
	  if (panel.style.display === "block") {
		panel.style.display = "none";
	  } else {
		panel.style.display = "block";
	  }
	});
  }
  
  var CLIENT_LOG_URL = "<?php echo url('client-log') ?>";
  var REQUEST_URI = "<?php echo $_SERVER['REQUEST_URI'];  ?>";
  
  //jQuery(window).load(function() {
    var dt = new Date();
    //alert(dt);
    jQuery.ajax({
            type: "GET",
            url: CLIENT_LOG_URL,
            data: { log_type: '1', client_time: dt , request_uri : REQUEST_URI },
    });
    //});
  </script>

</body>
</html>
