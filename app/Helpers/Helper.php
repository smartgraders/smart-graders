<?php
namespace App\Helpers;
use App\BlogCategory;
use App\CmsSetting;
use App\Country;
use App\Menu;
use App\PopularDestination;
use App\Region;
use App\TourPrice;
use App\User;
use App\UserLog;
use App\Tour;
use App\Review;
use App\Wishlist;
use Illuminate\Support\Facades\Auth;
use App\Language;
use App\LanguageTranslation;
use App\Locations;
use App\Highschool;
use DB;
use Session;


use Stevebauman\Location\Facades\Location;


class Helper
{


    public static function trans($sentance) {
        return "Ashutosh";
    }

    public static function dates($start_date)
    {
        $date = explode(",",$start_date);
        $dt = $dd = $mm = $res = [];
        $i=0;
        $str = '';
        foreach ($date as $value) {
            $mm[] = ' '.date('M ',strtotime($value));
            if($i==0) { 
                $str.=' '.date('M d',strtotime($value)); 
            }            
            else if($i>0 && $mm[$i-1]==$mm[$i]) {
                $str.= ' '.date(', d',strtotime($value));
            }
            else {
                $mm[0] = date('M ',strtotime($value));
                $str.= ' '.date('M d',strtotime($value));
            } 
            $dd[] = date('D',strtotime($value));
            $i++;
        }
        $days = implode(", ",$dd);
        
        return $res = ['days'=>$days, 'dates'=>$str];
    }

    public static function city($cityId){
        $city = Locations::where('id',$cityId)->first();
        return $city;
    }

    public static function school($schoolId){
        $school = Highschool::where('id',$schoolId)->first();
        return $school;
    }
    
    public static function students( $students=0 )
    {
        $list = explode(",",$students);
        //DB::enableQueryLog();
        $sts = User::whereIn('id',($list))->get();
        //dd(DB::getQueryLog()); 
        $str='<ul>';
        foreach($sts as $st) {
            $str.= '<li>'.$st->name.'</li>';
            //echo $st->name. "----";
        }
        $str.='</ul>';
        return $str;
    }

    public static function api_request($action, $vars=[], $type='GET')
    {
        $secretKey = env('APP_API_SECRET');;
        $uniqueString = time(); 
        $userAgent = $_SERVER['HTTP_USER_AGENT'];

        $xAuthorizationToken = md5( $secretKey . $uniqueString . $userAgent); 
        $xAuthorizationTime = $uniqueString;

        $ch = curl_init();

        $string = '';
        if($type=='GET') {
            if(!empty($vars)) {
                $string = "?";
                foreach($vars as $key=>$val) $str[]="$key=$val";
                $string.= implode('&', $str);
            }
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"GET");
        }
        else if($type=='POST'){
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$vars);
        }

        $param_string = implode('&',$vars);
        $url =  env('APP_API_URL').$action.$string;

        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $headers = [
            'X-Authorization-Token: ' . $xAuthorizationToken,
            'X-Authorization-Time: ' . $xAuthorizationTime,
            'User-Agent: '.$userAgent
        ];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $server_output = curl_exec ($ch);
        curl_close ($ch);

        //Get the output
        return $server_output;
    }

    public static function date_for_org($dates)
    {
        $exp = explode("/", $dates);
        return $exp[2]."-".$exp[1]."-".$exp[0];
    }

    public static function menu() 
    {
        $menus = Menu::where('status',1)->orderBy('position', 'ASC')->get();

        $string='<ul>';
        foreach($menus as $menu) {
            // $sub_menus = Helper::sub_menu( $menu->id );
            // if( count($sub_menus)>0  ) {
            //     $string.='<li class="ddown"><a href="javascript:;">'. ucfirst($menu->name) .' <i class="fa fa-angle-down"></i></a><ul>';
            //     foreach($sub_menus as $smenu) {
            //         $string.= '<li><a href="'. url($smenu->url). '">'. ucfirst($smenu->name) .'</a></li>';
            //     }
            //     $string.='</ul></li>';
            // }
            // else {
                $string.= '<li><a href="'. url($menu->url). '">'. ucfirst($menu->name) .'</a></li>';
            //}
        }
        $string.='</ul>';
        return $string;
    }

    public static function menu2() 
    {
        $menus = Menu::where('status',1)->where('parent_id',0)->orderBy('position', 'ASC')->get();

        $string='<ul>';
        foreach($menus as $menu) {
            $sub_menus = Helper::sub_menu( $menu->id );
            if( count($sub_menus)>0  ) {
                foreach($sub_menus as $smenu) {
                    $string.= '<li><a href="'. url($smenu->url). '">'. ucfirst($smenu->name) .'</a></li>';
                }
            }
            else {
                $string.= '<li><a href="'. url($menu->url). '">'. ucfirst($menu->name) .'</a></li>';
            }
        }
        $string.='</ul>';
        return $string;
    }

    public static function sub_menu($pid=0) 
    {
        $menus = Menu::where('status',1)->where('parent_id',$pid)->orderBy('position', 'ASC')->get();
        return $menus;
    }

    public static function setting()
    {
        $setting = CmsSetting::pluck('content', 'name')
                ->all();
        return $setting;
    }

    public static function cookie( $region='' )
    {
        $cookie_name = 'LOCATION';
        //if(!isset($_COOKIE[$cookie_name])) 
        //{
            if(!empty($region)) {
                //$ip = \Request::ip();;
                //$position = Location::get($ip);
                //$region = $position->regionName;
                //if(empty($region)) $region = 'Hamilton';
                setcookie($cookie_name, $region, time() + (86400 * 30), "/"); // 86400 = 1 day
            }
        //}
        return $_COOKIE[$cookie_name];
    }

    public static function sendMail($user, $subject, $message, $filename=[]) 
	{

        $from = ['info@smartgraders.com', 'Smart Graders'];

        //$MAILGUN_KEY='3c619bdc9a414de5bd9c7ae5a5d14840-d32d817f-a2fd0369';
        //$MAILGUN_DOMAIN='mg.smartgraders.com';
        //$MAILGUN_URL = 'https://api.mailgun.net/v3/sandboxb87baf9214d142e28711db94d53d516f.mailgun.org';
        
        $MAILGUN_KEY='6f58fcf8c98acf251ea9bde383b63470-d32d817f-687c4a3a';
        $MAILGUN_DOMAIN='mg.smartgraders.com';
        $MAILGUN_URL = 'https://api.mailgun.net/v3/'.$MAILGUN_DOMAIN.'/messages';
        
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_USERPWD, 'api:'.($MAILGUN_KEY));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		
		$plain 	 = strip_tags(nl2br($message));
		
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_URL, $MAILGUN_URL);
		
		if( !empty($filename) )
		$post_data = array(
							'from' 			=> "$from[1] <$from[0]>",
							'to' 			=> "$user[1] <$user[0]>",
							'subject' 		=> $subject,
							'html' 			=> $message,
							'text' 			=> $plain,
							'o:tracking'=>'yes',
							'o:tracking-clicks'=>'yes',
							'o:tracking-opens'=>'yes',
							'h:Reply-To'	=> $user[0], 
							'attachment[0]' =>  curl_file_create($filename[0], 'application/pdf', $filename[0])
						);
		else	
		$post_data = array(
							'from' 			=> "$from[1] <$from[0]>",
							'to' 			=> "$user[1] <$user[0]>",
							'subject' 		=> $subject,
							'html' 			=> $message,
							'text' 			=> $plain,
							'o:tracking'=>'yes',
							'o:tracking-clicks'=>'yes',
							'o:tracking-opens'=>'yes',
							'h:Reply-To'	=> $user[0] 
						);			
		
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		
		$res = curl_exec($ch);
		
		$result = json_decode( $res );
		
		curl_close($ch);
		return $result;
	}


    //Generated By Abhendra

    

    

    public static function popularDestination($orderId) {
        $popularDestination = PopularDestination::where('position',$orderId)
            ->get();
        return $popularDestination;
    }

    public static function tourDetails($id) {
        $tourDetails = Tour::with('tourPrice')->find($id);
        return $tourDetails;
    }

    public static function userDetail($user_id) {
        $user = User::select('name')
            ->where('id',$user_id)
            ->first();
        return $user->name;
    }

    public static function tourPrice($id) {
        $tourPrice = TourPrice::join('traveler_types','traveler_types.id','tour_prices.traveler_type_id')
            ->where('tour_id',$id)
            ->orderBy('price','ASC')
            ->get();
        return $tourPrice;
    }

    public static function tourPriceTraveler ($id) {
        $tourPrice = TourPrice::join('traveler_types','traveler_types.id','tour_prices.traveler_type_id')
            ->where('tour_id',$id)
            ->orderBy('price','ASC')
            ->pluck('tour_prices.price','traveler_types.name');
        return $tourPrice;

    }

    public static function searchResult() {
        $populerSearch = UserLog::select(DB::raw('count(id) as total_search, page_url'))
            ->where('page_url','like','%search%')
            ->orWhere('page_url','like','%location%')
            ->orWhere('page_url','like','%attractions%')
            ->groupBy('page_url')
            ->orderBy('total_search','DESC')
            ->take(10)
            ->get();
        return $populerSearch;
    }

    public static function featureTour() {
        $featuredTours = UserLog::select(DB::raw('count(id) as total_search, page_url'))
            ->where('page_url','like','%location%')
            ->groupBy('page_url')
            ->orderBy('total_search','DESC')
            ->take(10)
            ->get();
        return $featuredTours;
    }

    public static function review($tour_id) {
        $reviews = Review::where('tour_id',$tour_id)
                    ->pluck('rating','id')
                    ->toArray();
        if(count($reviews)>0) {
         $average = round(array_sum($reviews) / count($reviews));
        } else {
         $average = 0;
        }
        $otherStar = 5-$average;
        $rating = "";
        for($i=1; $i<=$average; $i++) {
            $rating.= '<i class="fa fa-star" aria-hidden="true"></i>';
        }
        for($i=1; $i<=$otherStar; $i++) {
            $rating.= '<i class="fa fa-star" style="color: #ccc;" aria-hidden="true"></i>';
        }
        $rating.= " (".count($reviews)." Review )";
        return $rating;
    }

    public static function countryName($country_id) {
        $country = Country::select('name')
            ->where('id',$country_id)
            ->first();
        return $country->name;
    }

    public static function provinceName($province_id) {
        $province = Region::select('name')
            ->where('id',$province_id)
            ->first();
        return $province->name;
    }

    public static function showWishlist($tour_id) {
      $wishlist = Wishlist::where('user_id',Auth::user()->id)
      ->where('tour_id',$tour_id)
      ->get();
      if(count($wishlist)>0) {
        return true;
      } else {
        return false;
      }
    }

    public static function blogCategory($categoryId){
       $categpry = BlogCategory::where('id',$categoryId)->first();
       return $categpry->category;
    }

}


?>
