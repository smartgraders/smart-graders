<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
       'current_package_id', 'first_name', 'last_name', 'email', 'phone', 'gender', 'address', 'city', 'province', 'dob', 'student_led', 'highschool'
    ];

    protected $table = 'tbl_student';
    //
}
