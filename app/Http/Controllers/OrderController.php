<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Helpers\Helper;
use Session;
use Stripe;
use App\User;
use App\Student;
use App\Programmes;
use App\ProgrammeAddon;
use App\ProgrammeEvent;
use App\Template;
use App\Locations;
use PDF;

class OrderController extends Controller
{
    public function report()
    {
        $student    = Student::Where('id', 6743)->first();
        $prgevent   = ProgrammeEvent::Where('id',$student->programme_id)->first();
        $programme  = Programmes::Where('id',$student->current_package_id)->first();
        $location   = Locations::where('id',$prgevent->city_id)->first();
        $location_list = Locations::Where('status',1)->orderBy('title','ASC')->get();

        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('frontend.order.report2', compact(['location_list', 'location', 'student', 'prgevent']));
        $file = 'docs/'.$student->first_name.'_'.$student->last_name.'_'.Session::get('session_id').'.pdf';
        return $pdf->stream('download.pdf');
    }

    public function stripePost(Request $request)
    {
        //$user_id  = $request->user_id;
        $user_id = Session::get('user_id');
        $stripe_token = $request->stripeToken;

        try {

            $student    = Student::Where('id', $user_id)->first();
            $prgevent   = ProgrammeEvent::Where('id',$student->programme_id)->first();
            $programme  = Programmes::Where('id',$student->current_package_id)->first();
            $location   = Locations::where('id',$prgevent->city_id)->first();
            $location_list = Locations::Where('status',1)->orderBy('title','ASC')->get();

            $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('frontend.order.report', compact(['location_list', 'location', 'student', 'prgevent']));
            //$file = 'docs/'.$student->first_name.'_'.$student->last_name.'_'.Session::get('session_id').'.pdf';
            $filename = str_replace(" ", "-", $student->first_name.'-'.$student->last_name.'-'.$student->id.'.pdf');
            $file = 'docs/'.$filename;
            $pdf->save( $file );

            $sum = $price = $programme->fee;
            if($student->offer_saving) {
                $offers = explode(",",$student->offer_saving);
                foreach($offers as $offer) {
                    $saving = explode("_",$offer);
                    if($saving[0]) {
                        $pa = ProgrammeAddon::Where('id',$saving[0])->first();
                        if($pa) {
                            //echo $pa->amount.'+';
                            $sum+=(float)$pa->amount;
                        }
                    }
                }
            }

            $tax = ($sum * 13)/100;
            $amount = $sum + $tax;
            $amount = round($amount,2);

            $order_id = substr(Session::get('session_id'),0,10);

            Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

            //add customer to stripe
            $customer = Stripe\Customer::create(array(
                'email' => $student->email,
                'source'  => $stripe_token
            ));

            Stripe\Charge::create ([
                'customer'      => $customer->id,
                "amount" => $amount * 100,
                "currency" => "cad",
                //"source" => $stripe_token,
                //'capture'       => false,
                "description" => 'Register for - '.$package->title,
                'metadata' => array(
                    'order_id' => $orderID
                )
            ]);
            
            Student::where('id',$user_id)->update(['document'=>$file, 'payment_status'=>1, 'payment_method'=>'Card']);

            $template = Template::where('slug','programme-and-fees-payment-confirm')->first();
            $message = $template->content;
            //For Event Detail
            $date = $prgevent->event_start_date.' - '.$prgevent->event_start_time.' To '.$prgevent->event_end_time;
            $message = str_replace('{{dates}}', $date, $message);
            //For Location Detail
            $message = str_replace('{{location}}', $location->address, $message);
            $message = str_replace('{{city}}', $location->city, $message);
            $message = str_replace('{{phone}}', $location->contact_no, $message);
            $message = str_replace('{{email}}', $location->email, $message);
            //For Student Detail
            $message = str_replace('{{name}}', $student->first_name.' '.$student->last_name, $message);
            $message = str_replace('{{first_name}}', $student->first_name, $message);
            $message = str_replace('{{last_name}}', $student->last_name, $message);
            $message = str_replace('{{address}}', $student->address, $message);
            $message = str_replace('{{phone2}}', $student->phone, $message);
            $message = str_replace('{{email2}}', $student->email, $message);
            $message = str_replace('{{dob}}', $student->dob, $message);
            $message = str_replace('{{g1g2}}', $student->g1g2, $message);
            $message = str_replace('{{expiry_date}}', $student->student_led, $message);
            $message = str_replace('{{package_name}}', $programme->title, $message);
            $message = str_replace('{{payment_mode}}', $request->payment_method, $message);
            
            Helper::sendMail([$student->email,$student->first_name], $template->subject, $message, [$file]);

            $template = Template::where('slug','programme-and-fees-payment-confirm-admin')->first();
            $message = $template->content;
            //For Event Detail
            $date = $prgevent->event_start_date.' - '.$prgevent->event_start_time.' To '.$prgevent->event_end_time;
            $message = str_replace('{{dates}}', $date, $message);
            //For Location Detail
            $message = str_replace('{{location}}', $location->address, $message);
            $message = str_replace('{{city}}', $location->city, $message);
            $message = str_replace('{{phone}}', $location->contact_no, $message);
            $message = str_replace('{{email}}', $location->email, $message);
            //For Student Detail
            $message = str_replace('{{name}}', $student->first_name.' '.$student->last_name, $message);
            $message = str_replace('{{first_name}}', $student->first_name, $message);
            $message = str_replace('{{last_name}}', $student->last_name, $message);
            $message = str_replace('{{address}}', $student->address, $message);
            $message = str_replace('{{phone2}}', $student->phone, $message);
            $message = str_replace('{{email2}}', $student->email, $message);
            $message = str_replace('{{dob}}', $student->dob, $message);
            $message = str_replace('{{g1g2}}', $student->g1g2, $message);
            $message = str_replace('{{expiry_date}}', $student->student_led, $message);
            $message = str_replace('{{package_name}}', $programme->title, $message);
            $message = str_replace('{{payment_mode}}', $request->payment_method, $message);
            
            $setting = \App\Helpers\Helper::setting();
            if((Session::get('city_slug')=='oakville')) {
                $email = ['g1g2drivingschooloakville@gmail.com', 'G1G2 Driving School'];
            }
            else {
                //$email = [ $setting['email_sender'], $setting['sender_name'] ];
                $city_slug = Session::get('city_slug');
                $location = Locations::Where('status',1)->where('slug',$city_slug)->first();
                $email = [$location->email, 'G1G2 '.$location->title.' Driving School'];
            }
            Helper::sendMail($email, $template->subject, $message, [$file]);
            Helper::sendMail(['jag6010@gmail.com', 'G1G2 '.$location->title.' Driving School'], $template->subject, $message, [$file]);

            //Helper::sendMail([$student->email,$student->first_name], $template->subject, $message, [$file]);

            Session::put('class_type','');
            Session::put('package','');
            Session::put('programme_id','');
            Session::put('user_id','');
            Session::regenerate();
            Session::put('session_id', Session::getId() );

            Session::flash('success', 'Payment successful!');
            return \redirect('payment-done');
        }
        catch(Stripe_CardError $e) {
            dd('card declined');
            return \redirect('payment-failed');
        }
    } 
    
    public function chequeCash(Request $request)
    {
        //$user_id  = $request->user_id;
        $user_id = Session::get('user_id');

        try {

            $student    = Student::Where('id', $user_id)->first();
            $prgevent   = ProgrammeEvent::Where('id',$student->programme_id)->first();
            $programme  = Programmes::Where('id',$student->current_package_id)->first();
            $location   = Locations::where('id',$prgevent->city_id)->first();
            $location_list = Locations::Where('status',1)->orderBy('title','ASC')->get();

            $pdf = PDF::loadView('frontend.order.report', compact(['location_list', 'location', 'student', 'prgevent']));

            $filename = str_replace(" ", "-", $student->first_name.'-'.$student->last_name.'-'.$student->id.'.pdf');
            $file = 'docs/'.$filename;
            $pdf->save( $file );

            $sum = $price = $programme->fee;
            if($student->offer_saving) {
                $offers = explode(",",$student->offer_saving);
                foreach($offers as $offer) {
                    $saving = explode("_",$offer);
                    if($saving[0]) {
                        $pa = ProgrammeAddon::Where('id',$saving[0])->first();
                        if($pa) {
                            //echo $pa->amount.'+';
                            $sum+=(float)$pa->amount;
                        }
                    }
                }
            }

            $tax = ($sum * 13)/100;
            $amount = $sum + $tax;
            $amount = round($amount,2);
            
            Student::where('id',$user_id)->update(['document'=>$file, 'payment_status'=>0, 'payment_method'=>$request->payment_method, 'message'=>$request->message]);

            $template = Template::where('slug','programme-and-fees-payment-confirm')->first();
            $message = $template->content;
            //For Event Detail
            $date = $prgevent->event_start_date.' - '.$prgevent->event_start_time.' To '.$prgevent->event_end_time;
            $message = str_replace('{{dates}}', $date, $message);
            //For Location Detail
            $message = str_replace('{{location}}', $location->address, $message);
            $message = str_replace('{{city}}', $location->city, $message);
            $message = str_replace('{{phone}}', $location->contact_no, $message);
            $message = str_replace('{{email}}', $location->email, $message);
            //For Student Detail
            $message = str_replace('{{name}}', $student->first_name.' '.$student->last_name, $message);
            $message = str_replace('{{first_name}}', $student->first_name, $message);
            $message = str_replace('{{last_name}}', $student->last_name, $message);
            $message = str_replace('{{address}}', $student->address, $message);
            $message = str_replace('{{phone2}}', $student->phone, $message);
            $message = str_replace('{{email2}}', $student->email, $message);
            $message = str_replace('{{dob}}', $student->dob, $message);
            $message = str_replace('{{g1g2}}', $student->g1g2, $message);
            $message = str_replace('{{expiry_date}}', $student->student_led, $message);
            $message = str_replace('{{package_name}}', $programme->title, $message);
            $message = str_replace('{{payment_mode}}', $request->payment_method, $message);
            
            Helper::sendMail([$student->email,$student->first_name], $template->subject, $message, [$file]);

            $template = Template::where('slug','programme-and-fees-payment-confirm-admin')->first();
            $message = $template->content;
            //For Event Detail
            $date = $prgevent->event_start_date.' - '.$prgevent->event_start_time.' To '.$prgevent->event_end_time;
            $message = str_replace('{{dates}}', $date, $message);
            //For Location Detail
            $message = str_replace('{{location}}', $location->address, $message);
            $message = str_replace('{{city}}', $location->city, $message);
            $message = str_replace('{{phone}}', $location->contact_no, $message);
            $message = str_replace('{{email}}', $location->email, $message);
            //For Student Detail
            $message = str_replace('{{name}}', $student->first_name.' '.$student->last_name, $message);
            $message = str_replace('{{first_name}}', $student->first_name, $message);
            $message = str_replace('{{last_name}}', $student->last_name, $message);
            $message = str_replace('{{address}}', $student->address, $message);
            $message = str_replace('{{phone2}}', $student->phone, $message);
            $message = str_replace('{{email2}}', $student->email, $message);
            $message = str_replace('{{dob}}', $student->dob, $message);
            $message = str_replace('{{g1g2}}', $student->g1g2, $message);
            $message = str_replace('{{expiry_date}}', $student->student_led, $message);
            $message = str_replace('{{package_name}}', $programme->title, $message);
            $message = str_replace('{{payment_mode}}', $request->payment_method, $message);
            
            $setting = \App\Helpers\Helper::setting();
            if((Session::get('city_slug')=='oakville')) {
                $email = ['g1g2drivingschooloakville@gmail.com', 'G1G2 Driving School'];
            }
            else {
                //$email = [ $setting['email_sender'], $setting['sender_name'] ];
                $city_slug = Session::get('city_slug');
                $location = Locations::Where('status',1)->where('slug',$city_slug)->first();
                $email = [$location->email, 'G1G2 '.$location->title.' Driving School'];
            }
            Helper::sendMail($email, $template->subject, $message, [$file]);

            Session::put('class_type','');
            Session::put('package','');
            Session::put('programme_id','');
            Session::put('user_id','');
            Session::regenerate();
            Session::put('session_id', Session::getId() );

            Session::flash('success', 'Order Confirmed!');
            return \redirect('payment-pending');
        }
        catch(Stripe_CardError $e) {
            return \redirect('payment-failed');
        }
    }
    
    public function paymentPending() {
        //$user_id = Session::get('user_id');
        return view('frontend.order.payment_pending');
    }

    public function paymentDone() {
        //$user_id = Session::get('user_id');
        return view('frontend.order.payment_done');
    }

    public function paymentFailed() {
        //$user_id = Session::get('user_id');
        return view('frontend.order.payment_fail');
    }

}
