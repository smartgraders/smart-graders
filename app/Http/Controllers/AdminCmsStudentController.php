<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use App\Helpers\Helper;
	use App\Student;
	use App\Instructor;
	use App\Lession;
	use App\ProgrammeEvent;
	use App\Locations;
	use Illuminate\Support\Facades\Redirect;

	class AdminCmsStudentController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "name";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = true;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = true;
			$this->button_show = true;
			$this->button_filter = true;
			$this->button_import = true;
			$this->button_export = true;
			$this->table = "cms_users";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			//$this->col[] = ["label"=>"Assign Teacher","name"=>"assign_id"];
			$this->col[] = ["label"=>"Name","name"=>"name"];
			$this->col[] = ["label"=>"Email","name"=>"email"];
			//$this->col[] = ["label"=>"Username","name"=>"username"];
			$this->col[] = ["label"=>"Grade","name"=>"grade","join"=>"tbl_class_type,title"];
			$this->col[] = ["label"=>"Status","name"=>"status","callback_php"=>'($row->status==1)?"<span class=\'label label-success\'>Active</span>":"<span class=\'label label-danger\'>Inactive</span>"'];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			//$this->form[] = ['label'=>'Assigned Teacher','name'=>'assign_id','type'=>'select2','validation'=>'required','width'=>'col-sm-10','datatable'=>'cms_users,name','datatable_where'=>'id_cms_privileges = 3','placeholder'=>'You can only enter the letter only'];
			$this->form[] = ['label'=>'Name','name'=>'name','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','help'=>'File types support : JPG, JPEG, PNG, GIF, BMP'];
			$this->form[] = ['label'=>'Email','name'=>'email','type'=>'email','validation'=>'required|min:1|max:255|email|unique:cms_users','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Username','name'=>'username','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10','help'=>'Minimum 5 characters. Please leave empty if you did not change the password.'];
			$this->form[] = ['label'=>'Password','name'=>'password','type'=>'password','validation'=>'min:3|max:32','width'=>'col-sm-10','placeholder'=>'You can only enter the number only'];
			$this->form[] = ['label'=>'Phone','name'=>'phone','type'=>'number','validation'=>'required|numeric','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Grade','name'=>'grade','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Grade','name'=>'grade','type'=>'select2','validation'=>'required','width'=>'col-sm-10','datatable'=>'tbl_class_type,title','datatable_where'=>'status = 1'];
			$this->form[] = ['label'=>'Photo','name'=>'photo','type'=>'upload','width'=>'col-sm-10','placeholder'=>'Please enter a valid email address'];
			$this->form[] = ['label'=>'Notes','name'=>'notes','type'=>'textarea','width'=>'col-sm-10','dataenum'=>'0|Inactive;1|Active'];
			$this->form[] = ['label'=>'Status','name'=>'status','type'=>'radio','validation'=>'required|min:1|max:255','width'=>'col-sm-10 inlineblock','dataenum'=>'0|Inactive;1|Active'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			////$this->form[] = ['label'=>'Assign Teacher','name'=>'assign_id','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Name','name'=>'name','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			//$this->form[] = ['label'=>'Photo','name'=>'photo','type'=>'upload','validation'=>'required|image|max:3000','width'=>'col-sm-10','help'=>'File types support : JPG, JPEG, PNG, GIF, BMP'];
			//$this->form[] = ['label'=>'Email','name'=>'email','type'=>'email','validation'=>'required|min:1|max:255|email|unique:cms_users','width'=>'col-sm-10','placeholder'=>'Please enter a valid email address'];
			//$this->form[] = ['label'=>'Username','name'=>'username','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Password','name'=>'password','type'=>'password','validation'=>'min:3|max:32','width'=>'col-sm-10','help'=>'Minimum 5 characters. Please leave empty if you did not change the password.'];
			////$this->form[] = ['label'=>'Token','name'=>'token','type'=>'text','validation'=>'required|min:1|max:255|unique:cms_users','width'=>'col-sm-10'];
			////$this->form[] = ['label'=>'Cms Privileges','name'=>'id_cms_privileges','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'cms_privileges,name'];
			//$this->form[] = ['label'=>'Phone','name'=>'phone','type'=>'number','validation'=>'required|numeric','width'=>'col-sm-10','placeholder'=>'You can only enter the number only'];
			//$this->form[] = ['label'=>'Grade','name'=>'grade','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Notes','name'=>'notes','type'=>'textarea','validation'=>'required|string|min:5|max:5000','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Status','name'=>'status','type'=>'radio','validation'=>'required|min:1|max:255','width'=>'col-sm-10','dataenum'=>'0|Inactive;1|Active'];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();
	        //$this->sub_module[] = ['label'=>'','path'=>'student/assign/[id]','button_color'=>'success','button_icon'=>'fa fa-user'];
	        if(CRUDBooster::myPrivilegeId()==1) {
				//$this->sub_module[] = ['label'=>'','path'=>'student/payment/[id]','button_color'=>'success','button_icon'=>'fa fa-dollar'];
				//$this->sub_module[] = ['label'=>'','path'=>'student/assign/[id]','button_color'=>'success','button_icon'=>'fa fa-user'];
			    //$this->sub_module[] = ['label'=>'','path'=>'https://www.g1-g2.com/docs/[session_id].pdf','button_color'=>'success','button_icon'=>'fa fa-file-pdf-o'];
			}
			else if(CRUDBooster::myPrivilegeId()==2) {
				//$this->sub_module[] = ['label'=>'','path'=>'student/class/[id]','button_color'=>'success','button_icon'=>'fa fa-play', 'button_title'=>'Class'];
				//$this->sub_module[] = ['label'=>'','path'=>'student/invoice/[id]','button_color'=>'warning','button_icon'=>'fa fa-file-pdf-o', 'button_title'=>'Class'];
			}


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();
	        if( CRUDBooster::myPrivilegeId()==1 || CRUDBooster::myPrivilegeId()==1 ) {
                $this->index_button[] = ['label'=>'Register','url'=>'cms-student/register','icon'=>'fa fa-user-plus'];
	        }


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        //$this->pre_index_html = null;
	        
	        $this->pre_index_html = '<a href="'.url('admin/cms-student/register').'" id="register" class="btn btn-primary btn-lg"><i class="fa fa-user-plus"></i> Register</a>';
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        $this->load_js[] = asset("js/slug.js");
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }
	    
	    public function getPause() 
		{
			//Create an Auth
			if(!CRUDBooster::isRead() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {    
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}
			
			Session::put('message', 'You have successfully paused!');
			return Redirect::to( url("/admin/cms-student/subscription") );
		}
		
		public function getStart() 
		{
			//Create an Auth
			if(!CRUDBooster::isRead() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {    
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}
			
			Session::put('message', 'You have successfully started!');
			return Redirect::to( url("/admin/cms-student/subscription") );
		}
		
		public function getStop() 
		{
			//Create an Auth
			if(!CRUDBooster::isRead() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {    
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}

			Session::put('message', 'You have successfully stoped!');
			return Redirect::to( url("/admin/cms-student/subscription") );
		}
		
	    
	    public function getSchedule() 
		{
			//Create an Auth
			if(!CRUDBooster::isRead() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {    
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}

			$data = $list = [];
			$data['page_title'] = 'Schedule';
			//DB::enableQueryLog();
			$data['batches'] = DB::table('tbl_batches')
			                    ->join('tbl_subject', function ($join) {
                                    $join->on('tbl_batches.batch_subject_id', '=', 'tbl_subject.id');
                                })
                                ->join('cms_users', function ($join) {
                                    $join->on('tbl_batches.teachers', '=', 'cms_users.id');
                                })
			                    ->where( 'students', '=',CRUDBooster::myId() )
			                    ->select('tbl_batches.*','tbl_subject.title','cms_users.name')
			                    ->get();
			 //dd(DB::getQueryLog());                      

			$this->cbView('backend.student_schedule', $data);
		}
	    
	    public function getSubscription() 
		{
			//Create an Auth
			if(!CRUDBooster::isRead() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {    
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}

			$data = $list = [];
			$data['page_title'] = 'Dashbaord';
			//DB::enableQueryLog();
			$data['subjects'] = DB::table('tbl_subject')
			                    ->join('tbl_subscriptions', function ($join) {
                                    $join->on('tbl_subject.id', '=', 'tbl_subscriptions.subject_id')
                                         ->where('tbl_subscriptions.student_id', '=', CRUDBooster::myId());
                                })
			                    ->select('tbl_subject.id', 'tbl_subject.title', 'tbl_subject.price', 'tbl_subscriptions.id as sub_id', 'tbl_subscriptions.cost', 'tbl_subscriptions.discount', 'tbl_subscriptions.total', 'tbl_subscriptions.status')
			                    ->get();
			 //dd(DB::getQueryLog());

			$this->cbView('backend.student_subsription', $data);
		}
	    
	    public function getRegister() 
		{
			//Create an Auth
			if(!CRUDBooster::isRead() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {    
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}

			$data = [];

			$this->cbView('backend.register', $data);
		}

		public function postRegister() 
		{
			//Create an Auth
			if(!CRUDBooster::isRead() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {    
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}

			if(isset($_POST['submit'])) 
			{
				if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) )
				{
					$template = DB::table('tbl_email_templates')->where('slug','invitation-email')->first();
					$message = $template->content;
					$message = str_replace('{{email}}', $location->email, $message);
					//For Post Detail
					$message = str_replace('{{TYPE}}', $_POST['type'], $message);
					$message = str_replace('{{EMAIL}}', $_POST['email'], $message);
					$message = str_replace('{{CODE}}', $_POST['code'], $message);
					
					$page = $_POST['type'] == 'Teacher' ? '/teacher-register' : '/student-register';
					
					$message = str_replace('{{LINK}}', url($page.'?token='.$_POST['code']), $message);
					
					$res = Helper::sendMail([ $_POST['email'], $_POST['type'] ], $template->subject, $message);

                    //CRUDBooster::insertLog(trans("crudbooster.register", ['email' => $_POST['email'], 'ip' => Request::server('REMOTE_ADDR')]));
					
					//print_r($res); exit;
					
					Session::put('message', '<div class="alert alert-success">You have sent('.$_POST['email'].') registration email successfully.</div>');
				}
				else {
					Session::put('message', '<div class="alert alert-warning">Invalid email.</div>'); 
				}
				return back();	
			}
		}
		
		public function getAssign($id) 
		{
			//Create an Auth
			if(!CRUDBooster::isRead() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {    
			  CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}
			
			$data = $list = [];
			$data['page_title'] = 'Assign Instructor';
			$data['row'] = DB::table('tbl_student')->where('id',$id)->first();
			$data['instructor'] = DB::table('cms_users')->where('id_cms_privileges',2)->orderBy('name','ASC')->get();

			//Please use cbView method instead view method from laravel
			$this->cbView('backend.assign_instructor',$data);
		}

		public function postAssign($id) 
		{
			//Create an Auth
			if(!CRUDBooster::isRead() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {    
			  CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}

			if(isset($_POST['submit'])) {
				DB::table('tbl_student')->where('id',$id)->update(['assigned_instructor_id' => $_POST['assigned_instructor_id']]);

				$student 	= DB::table('tbl_student')->where('id',$id)->first();
				$prgevent   = ProgrammeEvent::Where('id',$student->programme_id)->first();
				$location   = Locations::where('id',$prgevent->city_id)->first();
				$instructor = DB::table('cms_users')->where('id', $_POST['assigned_instructor_id'])->first();

				$template = DB::table('cms_email_templates')->where('slug','assign-instructor-notification-of-the-student')->first();
				$message = $template->content;
				$date = $prgevent->event_start_date.' - '.$prgevent->event_start_time.' To '.$prgevent->event_end_time;
				$message = str_replace('{{dates}}', $date, $message);
				//For Location Detail
				$message = str_replace('{{location}}', $location->address, $message);
				$message = str_replace('{{city}}', $location->city, $message);
				$message = str_replace('{{phone}}', $location->contact_no, $message);
				$message = str_replace('{{email}}', $location->email, $message);
				//For Student Detail
				$message = str_replace('{{student_name}}', $student->first_name.' '.$student->last_name, $message);
				$message = str_replace('{{first_name}}', $student->first_name, $message);
				$message = str_replace('{{last_name}}', $student->last_name, $message);
				$message = str_replace('{{address}}', $student->address, $message);
				$message = str_replace('{{phone}}', $student->phone, $message);
				$message = str_replace('{{email}}', $student->email, $message);
				$message = str_replace('{{dob}}', $student->dob, $message);
				$message = str_replace('{{g1g2}}', $student->g1g2, $message);
				$message = str_replace('{{expiry_date}}', $student->student_led, $message);
				$message = str_replace('{{package_name}}', $programme->title, $message);
				//For Instructor Detail
				$message = str_replace('{{instructor_name}}', $instructor->name, $message);
				
				Helper::sendMail([$student->email,$student->first_name], $template->subject, $message);

				$template = DB::table('cms_email_templates')->where('slug','assign-instructor-notification-of-the-instructor')->first();
				$message = $template->content;
				$date = $prgevent->event_start_date.' - '.$prgevent->event_start_time.' To '.$prgevent->event_end_time;
				$message = str_replace('{{dates}}', $date, $message);
				//For Location Detail
				$message = str_replace('{{location}}', $location->address, $message);
				$message = str_replace('{{city}}', $location->city, $message);
				$message = str_replace('{{phone}}', $location->contact_no, $message);
				$message = str_replace('{{email}}', $location->email, $message);
				//For Student Detail
				$message = str_replace('{{student_name}}', $student->first_name.' '.$student->last_name, $message);
				$message = str_replace('{{first_name}}', $student->first_name, $message);
				$message = str_replace('{{last_name}}', $student->last_name, $message);
				$message = str_replace('{{address}}', $student->address, $message);
				$message = str_replace('{{phone2}}', $student->phone, $message);
				$message = str_replace('{{email2}}', $student->email, $message);
				$message = str_replace('{{dob}}', $student->dob, $message);
				$message = str_replace('{{g1g2}}', $student->g1g2, $message);
				$message = str_replace('{{expiry_date}}', $student->student_led, $message);
				$message = str_replace('{{package_name}}', $programme->title, $message);
				//For Instructor Detail
				$message = str_replace('{{instructor_name}}', $instructor->name, $message);
				
				Helper::sendMail([$instructor->email,$instructor->name], $template->subject, $message);
				//Helper::sendMail(['jag6010@gmail.com', 'G1G2 '.$location->title.' Driving School'], $template->subject, $message);

				Session::put('message', '<div class="alert alert-success">Instructor assigned.</div>'); 
				
				$config['content'] = "Assign a instructor";
				$config['to'] = CRUDBooster::adminPath('student');
				$config['id_cms_users'] = [1,$instructor->id]; //This is an array of id users
				CRUDBooster::sendNotification($config);

				return back();
			}
		}


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	        $query->where('id_cms_privileges','4');       
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 
	        
	        $model = DB::table('cms_users')->where('id',$id)->first();
	        if($model->status==1) {
    	        $template = DB::table('cms_email_templates')->where('slug','student-login-details')->first();
    			$message = $template->content;
    			$message = str_replace('[NAME]', $model->name, $message);
    			$message = str_replace('[LINK]', 'http://smartgraders.com/ashu/admin', $message);
    			
    			$res = Helper::sendMail([$model->email, $model->name ], $template->subject, $message);
    			//print_r( $template); exit;
	        }

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }



	    //By the way, you can still create your own method in here... :) 


	}