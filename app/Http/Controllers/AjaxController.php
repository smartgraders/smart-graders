<?php

namespace App\Http\Controllers;

use App\Country;
use App\Region;
use App\User;
use App\Subscription;
use App\Student;
use App\ProfileType;
use App\Programmes;
use App\ProgrammeEvent;
use App\Template;
use App\Contact;
use App\UserLog;
use App\Locations;
use Browser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Stevebauman\Location\Facades\Location;
use Illuminate\Support\Facades\DB;
use App\Helpers\Helper;
use Session;

use Validator;

class AjaxController extends Controller
{
    
    public function contact_form(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name'      => 'required',
            'email'     => 'required|email',
            'phone'     => 'required',
            'subject'   => 'required',
            'message'   => 'required'
        ]);
        
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        else 
        {
            $model = new Contact;
            $model->name     = $request->name;
            $model->email    = $request->email;
            $model->phone    = $request->phone;
            $model->subject  = $request->subject;
            $model->message  = $request->message;

            if( $model->save() ) 
            {
                //$setting = \App\Helpers\Helper::setting();

                //For Admin Email
                $template = Template::where('slug','contact-form-for-admin')->first();
                $message = $template->content;
                $message = str_replace('[name]', $request->name, $message);
                $message = str_replace('[email]', $request->email, $message);
                $message = str_replace('[phone]', $request->phone, $message);
                $message = str_replace('[subject]', $request->subject, $message);
                $message = str_replace('[message]', $request->message, $message);
                
                $setting = \App\Helpers\Helper::setting();
                if((Session::get('city_slug')=='oakville')) {
                    $email = ['info@smartgraders.com', 'Smart Graders'];
                }
                else {
                    $city_slug = Session::get('city_slug');
                    $location = Locations::Where('status',1)->where('slug',$city_slug)->first();
                    $email = [$location->email, 'Smart Graders'];
                    //$email = [ $setting['email_sender'], $setting['sender_name'] ];
                }
                
                Helper::sendMail($email, $template->subject, $message);

                //For Customer Email Email
                $template = Template::where('slug','contact-form-for-online-class')->first();
                $message = $template->content;
                $message = str_replace('[name]', $request->name, $message);
                $message = str_replace('[email]', $request->email, $message);
                $message = str_replace('[phone]', $request->phone, $message);
                $message = str_replace('[subject]', $request->subject, $message);
                $message = str_replace('[message]', $request->message, $message);
                Helper::sendMail([$request->email,$request->name], $template->subject, $message);

                return response()->json(['success'=>'Success']);            
            }
        }
    }

    public function student_reg_form(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'code'             => 'required|unique:cms_users',
            'name'              => 'required',
            'email'             => 'required|email|unique:cms_users',
            'phone'             => 'required',
            'grade'             => 'required',
            //'username'          => 'required',
            'password'          => 'required|min:8',
            'confirm_password'  => 'required_with:password|same:password|min:8'
        ]);
        
        if ($validator->fails())
        {
            return response()->json(['type'=>'error', 'errors'=>$validator->errors()->all()]);
        }
        else 
        {
            $is_token_valid = User::where('code',$request->code)->first();
            if($is_token_valid) {
                return response()->json(['type'=>'error', 'msg'=>'Invalid token! Please contact our support.']);  
            }
            else
            {
                $model = new User;
                $model->name                = $request->name;
                $model->email               = $request->email;
                $model->phone               = $request->phone;
                $model->username            = $request->username;
                $model->password            = bcrypt($request->password);
                $model->grade               = $request->grade;
                $model->notes               = $request->note;
                $model->code                = $request->code;
                $model->id_cms_privileges   = 4;
                $model->status              = 0;
                $model->updated_at          = date('Y-m-d H:i:s');
                $model->created_at          = date('Y-m-d H:i:s');

                if( $model->save() ) 
                {
                    $subscription = new Subscription;
                    $subscription->student_id   = $model->id;
                    $subscription->grade_id     = $request->grade;
                    $subscription->status       = 0;
                    $subscription->updated_at   = date('Y-m-d H:i:s');
                    $subscription->created_at   = date('Y-m-d H:i:s');
                    $subscription->save();
                
                
                    //For Admin Email
                    $template = Template::where('slug','admin-email-for-student-registration')->first();
                    $message = $template->content;
                    $message = str_replace('[name]', $request->name, $message);
                    $message = str_replace('[email]', $request->email, $message);
                    $message = str_replace('[phone]', $request->phone, $message);
                    $message = str_replace('[username]', $request->username, $message);
                    $message = str_replace('[password]', $request->password, $message);
                    $message = str_replace('[grade]', $request->grade, $message);
                    $message = str_replace('[notes]', $request->notes, $message);
                    
                    $setting = \App\Helpers\Helper::setting();
                    $email = [ $setting['email_sender'], $setting['sender_name'] ];
                
                    Helper::sendMail($email, $template->subject, $message);

                    //For Customer Email Email
                    $template = Template::where('slug','customer-email-for-student-registration')->first();
                    $message = $template->content;
                    $message = str_replace('[name]', $request->name, $message);
                    $message = str_replace('[email]', $request->email, $message);
                    $message = str_replace('[phone]', $request->phone, $message);
                    $message = str_replace('[username]', $request->username, $message);
                    $message = str_replace('[password]', $request->password, $message);
                    $message = str_replace('[grade]', $request->grade, $message);
                    $message = str_replace('[notes]', $request->notes, $message);

                    Helper::sendMail([$request->email,$request->name], $template->subject, $message);

                    return response()->json(['type'=>'success', 'msg'=>'Congratulation! you have successfully registered.']);            
                }
            }
        }
    }

    public function teacher_reg_form(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'code'             => 'required|unique:cms_users',
            'name'             => 'required',
            'email'            => 'required|email|unique:cms_users',
            'phone'            => 'required',
            'teacher_grade'    => 'required',
            'teacher_subject'  => 'required',
            'wage_type'        => 'required',
            'wage_charge'      => 'required',
            //'username'          => 'required',
            'password'         => 'required|min:8',
            'confirm_password' => 'required_with:password|same:password|min:8'
        ]);
        
        if ($validator->fails())
        {
            return response()->json(['type'=>'error', 'errors'=>$validator->errors()->all()]);
        }
        else 
        {
            $is_token_valid = User::where('code',$request->code)->first();
            if($is_token_valid) {
                return response()->json(['type'=>'error', 'msg'=>'Invalid token! Please contact our support.']);  
            }
            else
            {
                $model = new User;
                $model->name                = $request->name;
                $model->email               = $request->email;
                $model->phone               = $request->phone;
                $model->username            = $request->username;
                $model->password            = bcrypt($request->password);
                
                $model->teacher_grade       = is_array($request->teacher_grade) ? implode(",",$request->teacher_grade) : $$request->teacher_grade;
                $model->teacher_subject     = is_array($request->teacher_subject) ? implode(",",$request->teacher_subject) : $$request->teacher_subject;
                $model->wage_type           = $request->wage_type;
                $model->wage_charge         = $request->wage_charge;
                
                $model->notes               = $request->note;
                $model->code                = $request->code;
                $model->id_cms_privileges   = 3;
                $model->status              = 0;

                if( $model->save() ) 
                {
                    //For Admin Email
                    $template = Template::where('slug','admin-email-for-teacher-registration')->first();
                    $message = $template->content;
                    $message = str_replace('[name]', $request->name, $message);
                    $message = str_replace('[email]', $request->email, $message);
                    $message = str_replace('[phone]', $request->phone, $message);
                    $message = str_replace('[username]', $request->username, $message);
                    $message = str_replace('[password]', $request->password, $message);
                    $message = str_replace('[hourly_wage]', $request->hourly_wage, $message);
                    $message = str_replace('[notes]', $request->notes, $message);
                    
                    $setting = \App\Helpers\Helper::setting();
                    $email = [ $setting['email_sender'], $setting['sender_name'] ];
                
                    Helper::sendMail($email, $template->subject, $message);

                    //For Customer Email Email
                    $template = Template::where('slug','customer-email-for-teacher-registration')->first();
                    $message = $template->content;
                    $message = str_replace('[name]', $request->name, $message);
                    $message = str_replace('[email]', $request->email, $message);
                    $message = str_replace('[phone]', $request->phone, $message);
                    $message = str_replace('[username]', $request->username, $message);
                    $message = str_replace('[password]', $request->password, $message);
                    $message = str_replace('[hourly_wage]', $request->hourly_wage, $message);
                    $message = str_replace('[notes]', $request->notes, $message);

                    Helper::sendMail([$request->email,$request->name], $template->subject, $message);

                    return response()->json(['type'=>'success', 'msg'=>'Congratulation! you have successfully registered.']);            
                }
            }
        }
    }

    public function programm_form(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'class_type'  => 'required',
            'package'   => 'required',
        ], [
            'class_type.required' => 'Please select class type',
            'package.required' => 'Please select programm type',
        ]);

        $res = response();

        if ($validator->fails()) {
            $res = $res->json(['errors'=>$validator->errors()->all()]);
        }
        else {
            Session::put('class_type',$request->class_type);
            Session::put('package',$request->package);
            $package   = Programmes::where('id',$request->package)->first();
            if($package->slug)
            $res = $res->json(['success'=>'Success', 'slug'=>url('program-and-fees/'.$package->slug.'/step1')]);
            else
            $res = $res->json(['error'=>'Invalid package seleted']);
        }
        return $res;
    }

    public function searchResult(Request $request) 
    {
        $tourLocation = Tour::where('location','like',"%".$request->keyword."%")
            ->groupBy('location')
            ->get();

        $tourtitle = Tour::join('tour_prices','tours.id','tour_prices.tour_id')
            ->where('title','like',"%".$request->keyword."%")
            ->groupBy('tours.id')
            ->get();

        $tourAttractions = Tour::where('attractions','like',"%".$request->keyword."%")
            ->groupBy('attractions')
            ->get();

        $data = "";
        $data.= "<div class=\"search_scroll\">";
        if(count($tourLocation)>0) {
            $data.= "<ul class=\"search_li marker_li\">";
            foreach($tourLocation as $location) {
                $locationUrl = urlencode($location->location);
                $data.= "<li><a href=\"/location/$locationUrl\">$location->location</a></li>";
            }
            $data.= "</ul>";
        }

        if(count($tourAttractions)>0) {
            $data.= "<ul class=\"search_li star_li\">";
            foreach($tourAttractions as $attractions) {
                $attractionsUrl = urlencode($attractions->attractions);
                $data.= "<li><a href=\"/attractions/$attractionsUrl\">$attractions->attractions</a></li>";
            }
            $data.= "</ul>";
        }

        if(count($tourtitle)>0) {
            $data.= "<ul class=\"search_li img_li\">";
            foreach($tourtitle as $title) {
                $url = url('/public/'.$title->images)."?w=75&h=50&fit=crop-center";
                $data.= "<li><a href=\"/tour/$title->slug\"><img src=\"$url\" alt=\"$title->title\"> $title->title
                    <div> From $ $title->price USD per person </div >
                    <div class=\"clearfix\" ></div>
                    </a>
                    </li>";
            }
            $data.= "</ul>";
        }
        $data.= "</div>
                 <div class=\"more_result\">
                  <a href=\"/tours/?search=$request->keyword\"><i class=\"fa fa-search\"></i> Find more results</a>
                </div>";
        echo $data;
    }

    public function searchBlogResult() {

      $blogs = Blog::join('blog_categories','blog_categories.id','blogs.category_id')
          ->where('title','like',"%".$request->keyword."%")
          ->get();

      if(count($blogs)>0) {
          $data.= "<ul class=\"search_li img_li\">";
          foreach($blogs as $blog) {
              $url = url('/public/'.$blog->cover_image)."?w=75&h=50&fit=crop-center";
              $data.= "<li><a href=\"/blog/$blog->slug\"><img src=\"$url\" alt=\"$blog->title\"> $blog->title
                  <div> $blog->category  $blog->slug</div >
                  <div class=\"clearfix\" ></div>
                  </a>
                  </li>";
          }
          $data.= "</ul>";
      }
      $data.= "</div>
               <div class=\"more_result\">
                <a href=\"/tours/?search=$request->keyword\"><i class=\"fa fa-search\"></i> Find more results</a>
              </div>";
      echo $data;

    }

    public function clienLog() 
    {
        if(Browser::isMobile()) {
            $platefrom = "Mobile";
        } elseif(Browser::isTablet()) {
            $platefrom = "Tablet";
        } elseif(Browser::isDesktop()) {
            $platefrom = "Desktop";
        } elseif(Browser::isBot()) {
            $platefrom = "Bot";
        } else {
            $platefrom = "Other";
        }
        if(Auth::check())
        {
            $user_id =  Auth::user()->id;
        } else
        {
            $user_id =  '0';
        }
        $ip = \Request::ip();;
        $position = Location::get($ip);

        //echo '<pre>'; print_r($position); print_r($ip); print_r($platefrom); exit;

        $userLog = new UserLog;
        $userLog->user_id          = $user_id;
        $userLog->session_id       = "0000000";
        $userLog->log_type         = $_GET['log_type'];
        $userLog->page_url         = $_GET['request_uri'];
        $userLog->ip_address       = $_SERVER['REMOTE_ADDR'];
        $userLog->country          = $position->countryCode;
        $userLog->city             = ($position->cityName);
        $userLog->isp              = "---";
        $userLog->platefrom        = $platefrom;
        $userLog->os               = Browser::platformName();
        $userLog->browser          = Browser::browserFamily();
        $userLog->browser_version  = Browser::browserVersion();
        $userLog->client_time      = $_GET['client_time'];
        $userLog->save();

    }

    public function country() {
        $countryList =  Country::orderBy('name','ASC')->pluck('name','id');
        return $countryList;
    }


    public function province() {
        $id = $_GET['country_id'];
        $cityList = Region::where('country_id',$id)
            ->orderBy('name','ASC')
            ->pluck('id','name');
        return $cityList;
    }


    public function tourVisit() {
        $tourId = $_GET['tourId'];
        $visite = Tour::where('id',$tourId)->increment('visit');
    }


}
