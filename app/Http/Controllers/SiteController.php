<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Stevebauman\Location\Facades\Location;
use App\FaqCategory;
use App\Helpers\Helper;
use App\Faq;
use App\Page;
use App\Locations;
use App\Programmes;
use App\ProgrammeType;
use App\ProgrammeEvent;
use App\ProgrammeAddon;
use App\Highschool;
use App\Testimonial;
use App\FAQs;
use App\Student;
use App\Template;
use App\Slider;
use App\ClassType;
use Session;
use Stripe;
use PDF;
use DB;

class SiteController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function student_reg()
    {
        $classes = DB::table('tbl_class_type')->orderBy('id')->get();
        return view('frontend.site.student_reg', compact(['classes']));
    }

    public function teacher_reg()
    {
        $subjects = DB::table('tbl_subject')->orderBy('title')->get();
        $classes = DB::table('tbl_class_type')->orderBy('id')->get();
        return view('frontend.site.teacher_reg', compact(['subjects','classes']));
    }

    public function secure_form()
    {
        $page = Page::Where('slug','secure-form')->first();

        if(!empty($_POST['stripeToken']))
        {
            extract($_POST);
            $order_id = substr(Session::get('session_id'),0,10);
            $stripe_token = $_POST['stripeToken'];
            $amount = str_replace(array("$",","),'',$amount);

            //$tax = ($amount * 13)/100;
            //$amount = $amount + $tax;
            //$amount = round($amount,2);

            //echo '<pre>'; print_r($_POST); exit;

            try {

                Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                //Stripe\Stripe::setApiKey('sk_test_51HA1z7APQIWdbWdYKPe719ZvWCwHrHyUyJRjI2lJp6h6wLM7gnRWXWX08E9hfylk36W3T57eyZzzW9nuXf3DcXfz007TOyOxhz');

                //add customer to stripe
                $customer = Stripe\Customer::create(array(
                    'email' => $email,
                    'source'  => $stripe_token
                ));

                //charge a credit or a debit card
                $charge = Stripe\Charge::create ([
                    'customer'      => $customer->id,
                    "amount"        => $amount * 100,
                    "currency"      => "cad",
                    //"source"        => $stripe_token,
                    //'capture'       => false,
                    "description"   => 'Secure Form Payment - '.$note,
                    'metadata' => array(
                        'order_id' => $orderID
                    )
                ]);

                $template = Template::where('slug','new-payment-from-secure-for-admin')->first();
                $message = $template->content;
                //For Event Detail
                $message = str_replace('{{name}}', $name, $message);
                $message = str_replace('{{phone}}', $phone, $message);
                $message = str_replace('{{email}}', $email, $message);
                $message = str_replace('{{notes}}', $note, $message);
                $message = str_replace('{{amount}}', $amount, $message);
                
                $city_slug = Session::get('city_slug');
                $location = Locations::Where('status',1)->where('slug',$city_slug)->first();
                $email = [$location->email, 'G1G2 '.$location->title.' Driving School'];
                Helper::sendMail($email, $template->subject, $message, [$file]);
                Helper::sendMail(['jag6010@gmail.com', 'G1G2 '.$location->title.' Driving School'], $template->subject, $message, [$file]);

                $template = Template::where('slug','new-payment-from-secure-form-for-customer')->first();
                $message = $template->content;
                //For Event Detail
                $message = str_replace('{{name}}', $name, $message);
                $message = str_replace('{{phone}}', $phone, $message);
                $message = str_replace('{{email}}', $email, $message);
                $message = str_replace('{{notes}}', $note, $message);
                $message = str_replace('{{amount}}', $amount, $message);
                
                $email = [$email, $name];
                Helper::sendMail($email, $template->subject, $message, [$file]);

                //echo '<pre>'; print_r($customer); print_r($charge); exit;
                return \redirect('payment-done');
            }
            catch(Stripe_CardError $e) {
                //echo '<pre>';  print_r($e); exit;
                dd('card declined');
                return \redirect('payment-failed');
            }
        }

        return view('frontend.site.secure_form', compact(['page']));
    }

    public function report()
    {
        $today = date('Y-m-10');
        $student = Student::Where( 'created_at', '>=', $today )->get();
        //print_r($student);

        $str = '<table border="1" cellpadding="10" cellspacing="0" width="100%">';
        $str.='<tr>';
            $str.='<th>Fullname</th>
                    <th>Email Address</th>
                    <th>Phone</th>
                    <th>Programme</th>
                    <th>Offer Seleted</th>
                    <th>HST (13%)</th>
                    <th>Amount</th>
                    <th>Payment Status</th>
                    ';
            $str.='</tr>';

            $tot = 0;
        foreach($student as $data) {
            
            $tax = $sum = $amount = 0;
            $programme  = Programmes::Where('id',$data->current_package_id)->first();
            $sum = $price = $programme->fee;
            if($data->offer_saving) {
                $offers = explode(",",$data->offer_saving);
                foreach($offers as $offer) {
                    $saving = explode("_",$offer);
                    if($saving[0]) {
                        $pa = ProgrammeAddon::Where('id',$saving[0])->first();
                        if($pa) {
                            $sum+=(float)$pa->amount;
                            $st[] = $pa->title.' - CAD $'.$pa->amount;
                        }
                    }
                }
            }

            $tax = ($sum * 13)/100;
            $amount = $sum + $tax;
            $amount = round($amount,2);

            if($data->payment_status)
            $tot+=$amount;

            $status = $data->payment_status ? 'Paid' : 'Pending';

            $str.='<tr>';
            $str.='<td>'.$data->first_name.' '.$data->last_name.'</td>
                    <td>'.$data->email.'</td>
                    <td>'.$data->phone.'</td>
                    <td>'.$programme->title .' - CAD $'.$programme->fee.'</td>
                    <td>'.implode('<br />',$st).'</td>
                    <td>CAD $'.number_format($tax,2).'</td>
                    <td>CAD $'.number_format($amount,2).'</td>
                    <td>'.$status.'</td>
                    ';
            $str.='</tr>';
        }

        $str.='<tr>
                    <th colspan="6" align="right">Total</th>
                    <th>CAD $'.number_format($tot,2).'</th>
                    <td></td>
                </tr>';

        $str.= '</table>';
        echo $str;

        $template = Template::where('slug','daily-report')->first();
        $message = $template->content;
        $message = str_replace('{{REPORT}}', $str, $message);
        
        $setting = \App\Helpers\Helper::setting();
        $city_slug = Session::get('city_slug');
        $location = Locations::Where('status',1)->where('slug',$city_slug)->first();
        $email = [$location->email, 'G1G2 '.$location->title.' Driving School'];
        Helper::sendMail($email, $template->subject, $message);

        //Helper::sendMail([ $setting['email_sender'], $setting['sender_name'] ], $template->subject, $message);

        //exit;
        // $location_list = Locations::Where('status',1)->orderBy('title','ASC')->get();
        // $pdf = PDF::loadView('frontend.site.pdfView', compact(['location_list']));
        // $file = Session::get('session_id').'.pdf';
        // $pdf->save( $file );
        //return $pdf->stream();

        //->save('myfile.pdf');

        //return $pdf->download('invoice.pdf');
        
        //return view('frontend.site.pdfView', compact(['location_list']));

    }

    public function index($slug = '') 
    {
        $view = 'frontend.site.index' ;

        return view($view);
    }

    public function location() 
    {
        $page = $location_list = $package_list = $testimonial_list = $faq_list = [];
        $page = Page::Where('slug','home')->first();
        $location_list = Locations::Where('status',1)->get();

        $region = Helper::cookie( $slug );

        return view('frontend.site.location',compact(['location_list']));
    }

    public function page($slug) 
    {
        $page = Page::Where('slug',$slug)->first();

        $testimonial_list = [];
        if( $page->is_testimonial )
        $testimonial_list   = Testimonial::Where('status',1)->get();
        
        $faq_list = [];
        if( $page->is_faq )
        $faq_list           = FAQs::Where('status',1)->get();

        $location_list = [];
        $location_list = Locations::Where('status',1)->get();
        
        return view('frontend.site.page', compact(['page', 'testimonial_list', 'faq_list', 'location_list']));
    }

    public function programme($slug='', $step='') 
    {
        $region = Helper::cookie( );

        if(empty($region))
        return redirect('/');
        
        $package = $prg_type = $events = $event = $package_addon = $highschool_list = $classtype = $res =[];

        $region = Helper::cookie();

        $page = Page::Where('slug','program-and-fees')->first();
        $location_detail    = Locations::Where('slug',$region)->first();

        $package = Programmes::Where('city_id',$location_detail->id);

        if(!empty($step) &&  empty(Session::get('class_type'))) {
            return redirect('program-and-fees'); 
        }
        
        if($slug) {

            if( Session::get('programme_id') ) {
                $event = ProgrammeEvent::Where('id',Session::get('programme_id'))->first();
            }

            if( Session::get('user_id') ) {
                $student = Student::Where('id', Session::get('user_id'))->first();
                $prgevent   = ProgrammeEvent::Where('id',$student->programme_id)->first();
                $programme  = Programmes::Where('id',$student->current_package_id)->first();
                
                $sum = $price = $programme->fee;
                if($student->offer_saving) {
                    $offers = explode(",",$student->offer_saving);
                    foreach($offers as $offer) {
                        $saving = explode("_",$offer);
                        if($saving[0]) {
                            $pa = ProgrammeAddon::Where('id',$saving[0])->first();
                            if($pa) {
                                $sum+=(float)$pa->amount;
                            }
                        }
                    }
                }

                $tax = ($sum * 13)/100;
                $amount = $sum + $tax;
                $amount = round($amount,2);
                $res = ['price'=>$price, 'tax'=>$tax, 'amount'=>$amount];
            }

            $package = $package->Where('slug',$slug)->first();
            $prg_type = ProgrammeType::get();
            if( count($prg_type)>0 ) {
                foreach($prg_type as $type) {
                    $events[$type->slug][] = ProgrammeEvent::Where('city_id',$location_detail->id)->Where('programme_id',$package->id)->Where('programme_type_id',$type->id)->orderBy('position','asc')->get();
                }
            }

            $location_list   = Locations::Where('status',1)->get();
            $package_addon   = ProgrammeAddon::Where('programme_id',$package->id)->get();
            $highschool_list = Highschool::Where('city_id',$location_detail->id)->get();
            if(count($highschool_list)==0)
            $highschool_list = Highschool::get();
        }
        else {
            $classtype  = ClassType::Where('status',1)->get();
            $package    = $package->get();
            Session::regenerate();
            Session::put('session_id', Session::getId() );
            Session::put('programme_id','');
        }
        
        if($step=='step1') {
            $view = 'frontend.site.program_step1';
        }
        else if($step=='step2') {
            $view = 'frontend.site.program_step2';
        }
        else if($step=='step3') {
            $view = 'frontend.site.program_step3';
        }
        else if($step=='final') {
            $view = 'frontend.site.program_final';
        }
        else {
            $view = 'frontend.site.program';
        }

        return view($view, compact(['page', 'slug', 'testimonial_list', 'faq_list', 'location_list', 'location_detail', 'package', 'prg_type', 'events', 'package_addon', 'highschool_list', 'event', 'classtype', 'res', 'student', 'prgevent']));
    }
    
    public function vclass() 
    {
        $page = Page::Where('slug','inclass-online-classroom-available')->first();
        return view('frontend.site.vclass',compact(['page']));
    }
    
    public function location2($slug) 
    {
        $region = Helper::cookie( $slug );
        Session::put('city_slug', $slug );
        return back();
        //return redirect('program-and-fees');
    }
    
    public function location3($slug) 
    {
        $region = Helper::cookie( $slug );
        Session::put('city_slug', $slug );
        //return back();
        return redirect('program-and-fees');
    }
    
    public function contact() 
    {
        $page = Page::Where('slug','contact-us')->first();
        $location_list = Locations::Where('status',1)->get();

        return view('frontend.site.contact',compact(['page', 'location_list']));
    }


}
