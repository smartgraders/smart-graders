<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;

	class AdminBatchesController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "batch_name";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = true;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = true;
			$this->button_show = true;
			$this->button_filter = true;
			$this->button_import = true;
			$this->button_export = true;
			$this->table = "tbl_batches";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Batch Name","name"=>"batch_name"];
			$this->col[] = ["label"=>"Batch Grade","name"=>"batch_grade_id","join"=>"tbl_class_type,title"];
			$this->col[] = ["label"=>"Batch Subject","name"=>"batch_subject_id","join"=>"tbl_subject,title"];
			$this->col[] = ["label"=>"Teacher","name"=>"teachers","join"=>"cms_users,name"];
			$this->col[] = ["label"=>"Students","name"=>"students","join"=>"cms_users,name", 'callback_php'=>'App\Helpers\Helper::students($row->students)'];
			$this->col[] = ["label"=>"Batch Start Date","name"=>"batch_start_date"];
			$this->col[] = ["label"=>"Batch Start Time","name"=>"batch_start_time"];
			$this->col[] = ["label"=>"Batch Days","name"=>"batch_days"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Batch Name','name'=>'batch_name','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Batch Grade','name'=>'batch_grade_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'tbl_class_type,title'];
			$this->form[] = ['label'=>'Batch Subject','name'=>'batch_subject_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'tbl_subject,title'];
			$this->form[] = ['label'=>'Teacher','name'=>'teachers','type'=>'select2','validation'=>'required','width'=>'col-sm-10','datatable'=>'cms_users,name','datatable_where'=>'id_cms_privileges = 3'];
			$this->form[] = ['label'=>'Students','name'=>'students','type'=>'select2multiple', 'multiple'=>true,'validation'=>'required','width'=>'col-sm-10','datatable'=>'cms_users,name','datatable_where'=>'id_cms_privileges = 4'];
			//$this->form[] = ['label'=>'Students','name'=>'students','type'=>'select2multiple','validation'=>'required','width'=>'col-sm-10','dataquery'=>'SELECT cms_users.name as label, cms_users.id as value FROM cms_users WHERE cms_users.grade = batch_grade_id AND cms_users.id_cms_privileges = 4'];
			$this->form[] = ['label'=>'Batch Start Date','name'=>'batch_start_date','type'=>'date','validation'=>'required|date','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Batch Start Time','name'=>'batch_start_time','type'=>'time','validation'=>'required|date_format:H:i:s','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Batch Days','name'=>'batch_days','type'=>'checkbox','validation'=>'required|min:1|max:255','width'=>'col-sm-10 inlineblock','dataenum'=>'Sun;Mon;Tue;Wed;Thu;Fri;Sat'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Batch Name','name'=>'batch_name','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Batch Grade Id','name'=>'batch_grade_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'tbl_class_type,title'];
			//$this->form[] = ['label'=>'Batch Subject Id','name'=>'batch_subject_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'tbl_subject,title'];
			//$this->form[] = ['label'=>'Students','name'=>'students','type'=>'select2multiple','validation'=>'required','width'=>'col-sm-10','datatable'=>'cms_users,name','datatable_where'=>'id_cms_privileges = 4'];
			//$this->form[] = ['label'=>'Batch Start Date','name'=>'batch_start_date','type'=>'date','validation'=>'required|date','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Batch Start Time','name'=>'batch_start_time','type'=>'time','validation'=>'required|date_format:H:i:s','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Batch Days','name'=>'batch_days','type'=>'checkbox','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        $this->load_js[] = asset("js/slug.js");
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }
	    
	    public function getselect_grade($id)
	    {
    		$data = DB::table('cms_users')
    		    ->select('cms_users.name as label','cms_users.id as value')
    			->where('grade', '=', $id)
    			->get();
    
    		return $data;
    	}


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	            
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here
	        $postdata['students'] = implode(",",$_POST['students']);
            return $postdata;
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here
	        
	        $model = DB::table('tbl_batches')
			                    ->join('tbl_subject', function ($join) {
                                    $join->on('tbl_batches.batch_subject_id', '=', 'tbl_subject.id');
                                  })
                                ->join('tbl_class_type', function ($join) {
                                    $join->on('tbl_batches.batch_grade_id', '=', 'tbl_class_type.id');
                                  })
			                    ->where( 'tbl_batches.id', '=',$id )
			                    ->select('tbl_batches.*','tbl_subject.title','tbl_class_type.title as classname')
			                    ->first();
			                    
			$config['content'] = "New batch ($model->batch_name) has been started for Grade - $model->title, Subject - $model->classname";
            $config['to'] = CRUDBooster::adminPath('cms-teacher/schedule');
            $config['id_cms_users'] = [$model->teachers]; //This is an array of id users
            CRUDBooster::sendNotification($config);   
            
            $config['content'] = "New batch ($model->batch_name) has been started for Grade - $model->title, Subject - $model->classname";
            $config['to'] = CRUDBooster::adminPath('cms-student/schedule');
            $config['id_cms_users'] = [$model->students]; //This is an array of id users
            CRUDBooster::sendNotification($config); 
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here
	        //echo '<pre>'; print_r($_POST); exit;
            $postdata['students'] = implode(",",$_POST['students']);
            return $postdata;
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 
            //DB::enableQueryLog();
	        $model = DB::table('tbl_batches')
			                    ->join('tbl_subject', function ($join) {
                                    $join->on('tbl_batches.batch_subject_id', '=', 'tbl_subject.id');
                                  })
                                ->join('tbl_class_type', function ($join) {
                                    $join->on('tbl_batches.batch_grade_id', '=', 'tbl_class_type.id');
                                  })
			                    ->where( 'tbl_batches.id', '=',$id )
			                    ->select('tbl_batches.*','tbl_subject.title','tbl_class_type.title as classname')
			                    ->first();
			                    
			$config['content'] = "New batch ($model->batch_name) has been started for Grade - $model->title, Subject - $model->classname";
            $config['to'] = CRUDBooster::adminPath('cms-teacher/schedule');
            $config['id_cms_users'] = [$model->teachers]; //This is an array of id users
            CRUDBooster::sendNotification($config);   
            
            $config['content'] = "New batch ($model->batch_name) has been started for Grade - $model->title, Subject - $model->classname";
            $config['to'] = CRUDBooster::adminPath('cms-student/schedule');
            $config['id_cms_users'] = [$model->students]; //This is an array of id users
            CRUDBooster::sendNotification($config); 
	        //dd(DB::getQueryLog()); 
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }



	    //By the way, you can still create your own method in here... :) 


	}