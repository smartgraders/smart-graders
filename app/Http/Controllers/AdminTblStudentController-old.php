<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use App\Helpers\Helper;
	use App\Student;
	use App\Instructor;
	use App\Lession;
	use App\ProgrammeEvent;
	use App\Locations;

	class AdminTblStudentController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "last_name";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = true;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = true;
			$this->button_show = true;
			$this->button_filter = true;
			$this->button_import = true;
			$this->button_export = true;
			$this->table = "tbl_student";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Name", "name"=>"last_name", "callback_php"=>'"<h3 class=\'title\'>".$row->first_name." ".$row->last_name."</h3><p><i class=\'fa fa-envelope-o\'></i> ".$row->email."</p><p><i class=\'fa fa-phone\'></i> ".$row->phone."</p><p><i class=\'fa fa-calendar\'></i> ".$row->prefered_start_date."</p>"'];
			$this->col[] = ["label"=>"Registered On","name"=>"created_at", "visible"=>true];
			//$this->col[] = ["label"=>"Course Start Date","name"=>"prefered_start_date", "visible"=>true];
			$this->col[] = ["label"=>"Email", "name"=>"email", "visible"=>true];
			//$this->col[] = ["label"=>"Name", "name"=>"last_name", "visible"=>true];
			$this->col[] = ["label"=>"First Name","name"=>"first_name", "visible"=>true];
			$this->col[] = ["label"=>"Phone","name"=>"phone", "visible"=>true];
			//$this->col[] = ["label"=>"Location","name"=>"city","join"=>"tbl_location,title", "visible"=>true];
			//$this->col[] = ["label"=>"Class Type","name"=>"class_type_id","join"=>"tbl_class_type,title", "visible"=>true];
			$this->col[] = ["label"=>"Assigned Instructor","name"=>"assigned_instructor_id","join"=>"cms_users,name", "visible"=>true];
			$this->col[] = ["label"=>"Payment Status","name"=>"payment_status","callback_php"=>'($row->payment_status==1)?"<span class=\'label label-success\'>Paid</span>":"<span class=\'label label-danger\'>Pending</span>"', "visible"=>true];
			$this->col[] = ["label"=>"Payment Method","name"=>"payment_method", "visible"=>true];
			//$this->col[] = ["label"=>"document","name"=>"document", "visible"=>true];
			//$this->col[] = ["label"=>"Download","name"=>"document", "callback"=>function($row) { 
				// 	if($row->payment_method!=NULL && !empty($row->document)) 
				// 		return '<a class=\'btn btn-xs btn-success\' title=\'\' href=\'https://www.g1-g2.com/'.$row->document.'\' target=\'_blank\'><i class=\'fa fa-file-pdf-o\'></i></a>'; 
				// 	elseif($row->payment_method!=NULL && !empty($row->session_id)) 
				// 		return '<a class=\'btn btn-xs btn-success\' title=\'\' href=\'https://www.g1-g2.com/docs/'.$row->first_name.'_'.$row->last_name.'_'.$row->session_id.'.pdf\' target=\'_blank\'><i class=\'fa fa-file-pdf-o\'></i></a>'; 
				// },"visible"=>false ];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Last Name','name'=>'last_name','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'First Name','name'=>'first_name','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Middle Name','name'=>'middle_name','type'=>'text','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Address','name'=>'address','type'=>'textarea','validation'=>'required|string|min:5|max:5000','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'City','name'=>'city','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'tbl_location,title'];
			$this->form[] = ['label'=>'Province','name'=>'province','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Postal Code','name'=>'postal_code','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Phone','name'=>'phone','type'=>'number','validation'=>'required|numeric','width'=>'col-sm-10','placeholder'=>'You can only enter the number only'];
			$this->form[] = ['label'=>'Email','name'=>'email','type'=>'email','validation'=>'required|min:1|max:255|email|unique:tbl_student','width'=>'col-sm-10','placeholder'=>'Please enter a valid email address'];
			$this->form[] = ['label'=>'Phone2','name'=>'phone2','type'=>'text','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Fax','name'=>'fax','type'=>'text','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Gender','name'=>'gender','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Dob','name'=>'dob','type'=>'date','validation'=>'required|date','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'G1g2','name'=>'g1g2','type'=>'text','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Student Led','name'=>'student_led','type'=>'date','validation'=>'required|date','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Prefered Start Date','name'=>'prefered_start_date','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Highschool','name'=>'highschool','type'=>'select2','validation'=>'required|min:1|max:255','width'=>'col-sm-10','datatable'=>'tbl_highschool,name'];
			$this->form[] = ['label'=>'Where did you find us?','name'=>'wfus','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Programme','name'=>'current_package_id','type'=>'select2','validation'=>'required|integer','width'=>'col-sm-10','datatable'=>'tbl_programme,title'];
			$this->form[] = ['label'=>'Class Type','name'=>'class_type_id','type'=>'select2','validation'=>'required|integer','width'=>'col-sm-10','datatable'=>'tbl_class_type,title'];
			$this->form[] = ['label'=>'Assigned Instructor','name'=>'assigned_instructor_id','type'=>'select2','validation'=>'required|integer','width'=>'col-sm-10','datatable'=>'cms_users,name'];
			$this->form[] = ['label'=>'Followed Up','name'=>'followed_up','type'=>'radio','width'=>'col-sm-10','dataenum'=>'1|Active; 0|Inactive'];
			$this->form[] = ['label'=>'Payment Status','name'=>'payment_status','type'=>'radio','validation'=>'required|integer|min:0','width'=>'col-sm-10','dataenum'=>'1|Paid; 0|Unpaid'];
			$this->form[] = ['label'=>'Payment Method','name'=>'payment_method','type'=>'radio','validation'=>'required','width'=>'col-sm-10','dataenum'=>'Cheque;Cash;Card'];
			//$this->form[] = ['label'=>'Document','name'=>'document','type'=>'text','validation'=>'required','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Hmaster Programme Pay Price','name'=>'hid_master_programme_pay_price','type'=>'number','width'=>'col-sm-10'];
			# END FORM DO NOT REMOVE THIS LINE

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
			*/
			if(CRUDBooster::myPrivilegeId()==1) {
				$this->sub_module[] = ['label'=>'','path'=>'student/payment/[id]','button_color'=>'success','button_icon'=>'fa fa-dollar'];
				$this->sub_module[] = ['label'=>'','path'=>'student/assign/[id]','button_color'=>'success','button_icon'=>'fa fa-user'];
			//$this->sub_module[] = ['label'=>'','path'=>'https://www.g1-g2.com/docs/[session_id].pdf','button_color'=>'success','button_icon'=>'fa fa-file-pdf-o'];
			}
			else if(CRUDBooster::myPrivilegeId()==2) {
				$this->sub_module[] = ['label'=>'','path'=>'student/class/[id]','button_color'=>'success','button_icon'=>'fa fa-play', 'button_title'=>'Class'];
				$this->sub_module[] = ['label'=>'','path'=>'student/invoice/[id]','button_color'=>'warning','button_icon'=>'fa fa-file-pdf-o', 'button_title'=>'Class'];
			}

	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();
			$this->index_button[] = ['label'=>'Register','url'=>'student/register','icon'=>'fa fa-user-plus'];



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();
			//if(CRUDBooster::myPrivilegeId()==2) {
				$this->index_statistic[] = ['label'=>'Students','count'=> Student::where('assigned_instructor_id', CRUDBooster::myId())->count(),'icon'=>'fa fa-users','color'=>'success'];
				$this->index_statistic[] = ['label'=>'Teachers','count'=> Instructor::count(),'icon'=>'fa fa-lock','color'=>'warning'];
				//$this->index_statistic[] = ['label'=>'Register','href'=>'ww', 'count'=>'+', 'icon'=>'fa fa-plus','color'=>'success'];
			//}


	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = '<a href="'.url('admin/student/register').'" id="register" class="btn btn-primary btn-sm"><i class="fa fa-user-plus"></i> Register</a>';
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js[] = asset("js/timer.js");
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
		}

		public function getRegister() 
		{
			//Create an Auth
			if(!CRUDBooster::isRead() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {    
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}

			$data = [];

			$this->cbView('backend.register', $data);
		}

		public function postRegister() 
		{
			//Create an Auth
			if(!CRUDBooster::isRead() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {    
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}

			if(isset($_POST['submit'])) 
			{
				if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) )
				{
					$template = DB::table('cms_email_templates')->where('slug','invitation-email')->first();
					$message = $template->content;
					$message = str_replace('{{email}}', $location->email, $message);
					//For Post Detail
					$message = str_replace('{{TYPE}}', $_POST['type'], $message);
					$message = str_replace('{{EMAIL}}', $_POST['email'], $message);
					$message = str_replace('{{CODE}}', $_POST['code'], $message);
					
					$page = $_POST['type'] == 'Teacher' ? '/teacher-register' : '/student-register';
					
					$message = str_replace('{{LINK}}', url($page.'?token='.$_POST['code']), $message);
					
					Helper::sendMail([$_POST['email'],'Guest'], $template->subject, $message);
				}
				else {
					Session::put('message', '<div class="alert alert-success">You have sent registration email successfully.</div>'); 
				}
				return back();	
			}
		}

		public function getInvoice($id) 
		{
			//Create an Auth
			if(!CRUDBooster::isRead() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {    
			  CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}
			
			$data = $data['list'] = $data['lessons_list'] = [];
			
			$data['row'] = Student::where('id',$id)->first(); 
			$data['page_title'] = 'Send invoice for completed Lesson';
			$data['instructor'] = Instructor::where('id_cms_privileges',2)->orderBy('name','ASC')->get();
			$data['lessons'] 	= Lession::where('student_id',$id)->where('instructor_id', $data['row']->assigned_instructor_id)->where('status','Completed')->get();
			foreach($data['lessons'] as $les) $data['list'][] = $les->lession;


			$data['invoices'] 	= DB::table('tbl_instructor_invoice')->where('student_id',$id)->where('instructor_id', $data['row']->assigned_instructor_id)->get();
			foreach($data['invoices'] as $les) {
				$l = explode(",",$les->lessions);
				if(is_array($l)) {
					foreach($l as $val) $data['lessons_list'][] = $val;
				}
				else {
					$data['lessons_list'][] = $les->lessions; 
				}
			}
			//$lessons_list = array_unique($data['lessons_list']);			
			//echo '<pre>'; print_r($data['lessons_list']);
			//exit;

			if(!empty(Session::get('student_lessons_id')))
			$data['lesson'] = Lession::where( 'id', Session::get('student_lessons_id') )->first();;

			//Please use cbView method instead view method from laravel
			$this->cbView('backend.invoice_instructor',$data);
		}

		public function postInvoice($id) 
		{
			//Create an Auth
			if(!CRUDBooster::isRead() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {    
			  CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}

			if(isset($_POST['submit'])) 
			{
				if(!empty($_POST['lession'])) {
					$lessions = implode(",",$_POST['lession']);
					$student = Student::where('id',$id)->first();
					$instructor = Instructor::where('id',$student->assigned_instructor_id)->first();

					$tbl_instructor_invoice = DB::table('tbl_instructor_invoice')
						->where('student_id' , $id)
						->where('instructor_id' , CRUDBooster::myId())
						->where('lessions' , $lessions)
						->first();

					if($tbl_instructor_invoice->lessions) {
						Session::put('message', '<div class="alert alert-success">Invoice already created.</div>'); 
					}					
					else {
						$hourly_wage = $instructor->hourly_wage;
						$minute_wage = $instructor->hourly_wage/60;
						
						$rows = Lession::select( 'total_time' )
								->where('student_id', $id)
								->where('instructor_id', $student->assigned_instructor_id)
								->where('status', 'Completed')
								//->whereIn('lession', [$lessions])
								->get();
						//echo '<pre>'; print_r($rows); //exit;
						$sum = 0;
						foreach($rows as $row) {
							$time = explode(":",$row->total_time);
							if($time[0] > 0) $sum += ($time[0]*60);
							if($time[1] > 0) $sum += ($time[1]);
							if($time[2] > 0) $sum += ($time[2]/60);
						}

						$total_charge = $sum * $minute_wage;

						DB::table('tbl_instructor_invoice')->insert([
							'student_id' 	  => $id, 
							'instructor_id'   => CRUDBooster::myId(), 
							'lessions' 		  => $lessions, 
							'total_charges'   => $total_charge,
							'status' 		  => 'Progress', 
							'updated_at' 	  => date('Y-m-d H:i:s'),
							'created_at' 	  => date('Y-m-d H:i:s'),
						]);
						$lessions = str_replace('1000', 'Extra', $lessions);
						$config['content'] = $student->first_name." created invoice for Lesson $lessions";
						$config['to'] = CRUDBooster::adminPath('instructor_invoice');
						$config['id_cms_users'] = [1,CRUDBooster::myId()]; //This is an array of id users
						CRUDBooster::sendNotification($config);
						Session::put('message', '<div class="alert alert-success">Your invoice is created now.</div>'); 
					}
				}
				else {
					Session::put('message', '<div class="alert alert-success">Please selecte lesson for invoice.</div>'); 
				}
				return back();	
			}
		}

		public function getClass($id) 
		{
			//Create an Auth
			if(!CRUDBooster::isRead() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {    
			  CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}
			
			$data = $list = [];
			
			$data['row'] 		= DB::table('tbl_student')->where('id',$id)->first();
			$data['page_title'] = 'Start Lesson for '.$data['row']->first_name.' '.$data['row']->last_name;
			$data['instructor'] = DB::table('cms_users')->where('id_cms_privileges',2)->orderBy('name','ASC')->get();
			$data['lessons'] 	= DB::table('tbl_student_lessons')->where('student_id',$id)->where('instructor_id', $data['row']->assigned_instructor_id)->where('status','Completed')->get();

			foreach($data['lessons'] as $les) $data['list'][] = $les->lession;

			//echo '<pre>'; print_r($data['list']);
			//exit;

			if(!empty(Session::get('student_lessons_id')))
			$data['lesson'] = DB::table('tbl_student_lessons')->where( 'id', Session::get('student_lessons_id') )->first();;

			//Please use cbView method instead view method from laravel
			$this->cbView('backend.class_instructor',$data);
		}

		public function postClass($id) 
		{
			//Create an Auth
			if(!CRUDBooster::isRead() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {    
			  CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}
			
			if(isset($_POST['start'])) {
				//DB::table('bl_student_lessons')->where('id',$id)->update(['assigned_instructor_id' => $_POST['assigned_instructor_id']]);

				DB::table('tbl_student_lessons')->insert([
					'student_id' 	  => $id, 
					'instructor_id'   => CRUDBooster::myId(), 
					'lession' 		  => $_POST['lession'], 
					'status' 		  => 'Progress', 
					'start_date_time' => date('Y-m-d H:i:s'),
					'end_date_time'   => date('Y-m-d H:i:s'),
					'updated_at' 	  => date('Y-m-d H:i:s'),
					'created_at' 	  => date('Y-m-d H:i:s'),
				]);

				Session::put('student_lessons_id', DB::getPdo()->lastInsertId()); 
				Session::put('message', '<div class="alert alert-warning">Your time is started now.</div>'); 

				return back();
			}

			if(isset($_POST['end'])) {

				DB::table('tbl_student_lessons')->where('id', Session::get('student_lessons_id'))
				->update([
					'status' 		=> 'Completed', 
					'total_time' 	=> $_POST['total_time'],
					'end_date_time' => date('Y-m-d H:i:s'),
					'updated_at' 	=> date('Y-m-d H:i:s'),
				]);
				Session::put('student_lessons_id', ''); 
				Session::put('message', '<div class="alert alert-success">Your time is stoped now.</div>'); 

				return back();
			}

			//Please use cbView method instead view method from laravel
			$this->cbView('backend.class_instructor',$data);
		}

		public function getAssign($id) 
		{
			//Create an Auth
			if(!CRUDBooster::isRead() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {    
			  CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}
			
			$data = $list = [];
			$data['page_title'] = 'Assign Instructor';
			$data['row'] = DB::table('tbl_student')->where('id',$id)->first();
			$data['instructor'] = DB::table('cms_users')->where('id_cms_privileges',2)->orderBy('name','ASC')->get();

			//Please use cbView method instead view method from laravel
			$this->cbView('backend.assign_instructor',$data);
		}

		public function postAssign($id) 
		{
			//Create an Auth
			if(!CRUDBooster::isRead() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {    
			  CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}

			if(isset($_POST['submit'])) {
				DB::table('tbl_student')->where('id',$id)->update(['assigned_instructor_id' => $_POST['assigned_instructor_id']]);

				$student 	= DB::table('tbl_student')->where('id',$id)->first();
				$prgevent   = ProgrammeEvent::Where('id',$student->programme_id)->first();
				$location   = Locations::where('id',$prgevent->city_id)->first();
				$instructor = DB::table('cms_users')->where('id', $_POST['assigned_instructor_id'])->first();

				$template = DB::table('cms_email_templates')->where('slug','assign-instructor-notification-of-the-student')->first();
				$message = $template->content;
				$date = $prgevent->event_start_date.' - '.$prgevent->event_start_time.' To '.$prgevent->event_end_time;
				$message = str_replace('{{dates}}', $date, $message);
				//For Location Detail
				$message = str_replace('{{location}}', $location->address, $message);
				$message = str_replace('{{city}}', $location->city, $message);
				$message = str_replace('{{phone}}', $location->contact_no, $message);
				$message = str_replace('{{email}}', $location->email, $message);
				//For Student Detail
				$message = str_replace('{{student_name}}', $student->first_name.' '.$student->last_name, $message);
				$message = str_replace('{{first_name}}', $student->first_name, $message);
				$message = str_replace('{{last_name}}', $student->last_name, $message);
				$message = str_replace('{{address}}', $student->address, $message);
				$message = str_replace('{{phone}}', $student->phone, $message);
				$message = str_replace('{{email}}', $student->email, $message);
				$message = str_replace('{{dob}}', $student->dob, $message);
				$message = str_replace('{{g1g2}}', $student->g1g2, $message);
				$message = str_replace('{{expiry_date}}', $student->student_led, $message);
				$message = str_replace('{{package_name}}', $programme->title, $message);
				//For Instructor Detail
				$message = str_replace('{{instructor_name}}', $instructor->name, $message);
				
				Helper::sendMail([$student->email,$student->first_name], $template->subject, $message);

				$template = DB::table('cms_email_templates')->where('slug','assign-instructor-notification-of-the-instructor')->first();
				$message = $template->content;
				$date = $prgevent->event_start_date.' - '.$prgevent->event_start_time.' To '.$prgevent->event_end_time;
				$message = str_replace('{{dates}}', $date, $message);
				//For Location Detail
				$message = str_replace('{{location}}', $location->address, $message);
				$message = str_replace('{{city}}', $location->city, $message);
				$message = str_replace('{{phone}}', $location->contact_no, $message);
				$message = str_replace('{{email}}', $location->email, $message);
				//For Student Detail
				$message = str_replace('{{student_name}}', $student->first_name.' '.$student->last_name, $message);
				$message = str_replace('{{first_name}}', $student->first_name, $message);
				$message = str_replace('{{last_name}}', $student->last_name, $message);
				$message = str_replace('{{address}}', $student->address, $message);
				$message = str_replace('{{phone2}}', $student->phone, $message);
				$message = str_replace('{{email2}}', $student->email, $message);
				$message = str_replace('{{dob}}', $student->dob, $message);
				$message = str_replace('{{g1g2}}', $student->g1g2, $message);
				$message = str_replace('{{expiry_date}}', $student->student_led, $message);
				$message = str_replace('{{package_name}}', $programme->title, $message);
				//For Instructor Detail
				$message = str_replace('{{instructor_name}}', $instructor->name, $message);
				
				Helper::sendMail([$instructor->email,$instructor->name], $template->subject, $message);
				//Helper::sendMail(['jag6010@gmail.com', 'G1G2 '.$location->title.' Driving School'], $template->subject, $message);

				Session::put('message', '<div class="alert alert-success">Instructor assigned.</div>'); 
				
				$config['content'] = "Assign a instructor";
				$config['to'] = CRUDBooster::adminPath('student');
				$config['id_cms_users'] = [1,$instructor->id]; //This is an array of id users
				CRUDBooster::sendNotification($config);

				return back();
			}
		}
		
		public function getPayment($id) 
		{
			//Create an Auth
			if(!CRUDBooster::isRead() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {    
			  CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}
			
			$data = [];
			$data['page_title'] = 'Payment History';
			$data['row'] = DB::table('tbl_student')->where('id',$id)->first();

			
			if($data['row']) {

				$data['programme'] = DB::table('tbl_programme')->where('id', $data['row']->current_package_id)->first();

				$sum = $price = $data['programme']->fee;

				if($data['row']->offer_saving) {
					$offers = explode(",", substr($data['row']->offer_saving, 1) );
					foreach($offers as $offer) {
						$saving = explode("_",$offer);
						if($saving[0]) {
							$pa = DB::table('tbl_programme_addon')->where('id',$saving[0])->first();
							if($pa) {
								$data['programme_addon'][] = $pa;
								$sum+=(float)$pa->amount;
							}
						}
					}
				}
			}
			$tax = ($sum * 13)/100;
			$amount = $sum + $tax;
			$amount = round($amount,2);

			$data['sum'] = $sum;
			$data['tax'] = $tax;
			$data['amount'] = $amount;

			//Please use cbView method instead view method from laravel
			$this->cbView('backend.payment_history',$data);
		}


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
			//Your code here
			if(CRUDBooster::myPrivilegeId()>1)
	        $query->where('assigned_instructor_id',CRUDBooster::myId());    
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }



	    //By the way, you can still create your own method in here... :) 


	}