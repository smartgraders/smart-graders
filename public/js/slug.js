
$("#title").keyup(function(){
    var Text = $(this).val();
    Text = Text.toLowerCase();
    Text = Text.replace(/[^a-zA-Z0-9]+/g,'-');
    $("#slug").val(Text);
});


$("#name").keyup(function(){
    var Text = $(this).val();
    Text = Text.toLowerCase();
    Text = Text.replace(/[^a-zA-Z0-9]+/g,'-');
    $("#slug").val(Text);
});

$("#cost, #discount").keyup(function(){
    var cost = parseFloat( $('#cost').val() );
    var discount = $('#discount').val() ? parseFloat( $('#discount').val() ) : 0;
    
    var percent = (cost * discount) / 100; 
    var total = cost - percent;
    
    $("#total").val(total);
});

$('#batch_grade_id').change(function(){
    var the = $(this);
    $('#students').html($('<option></option>').attr('value',0).text('Selet'));
    $.ajax({
		type: 'GET',
		url: '/ashu/admin/batches/select_grade/' + the.val(),
		data: '',
		success: function(result) {
			$.each(result, function(i, val){
			    //console.log(val);
				$('#students').html($('<option></option>').attr('value',val.value).text(val.label)); 
			});
		}
	});
})